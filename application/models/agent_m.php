<?php
class Agent_m extends CI_Model
{
	public function agentlogin($user,$pass)
	{
		$this->db->where('agentid',$user);
		$this->db->where('password',$pass);
		$this->db->where('status', 0);
		$this->db->where('xdelete', 0);
		$query=$this->db->get('agentmaster');
		return $result = $query->result();
	}
	
	public function LastLoginTimeUpdate($id,$date)
	{
		$this->db->set('lastlogintime', $date);
		$this->db->where('id',$id);
		$this->db->update('usermaster');
	}
	
	public function getUserDetails($username)
	{
		$this->db->select('*');
		$this->db->where('username', $username);
		$query=$this->db->get('usermaster');
		return $result = $query->result();
	}
	
	public function InsertnewTopic($data)
	{
		$this->db->insert('topicmaster',$data);
		$last_id = $this->db->insert_id();
		return $last_id;
	}
	
	public function getAllTopic()
	{
		$this->db->select('*');
		$this->db->where('ststus', 0);
		$this->db->where('xdelete', 0);
		$this->db->from('topicmaster');
		$this->db->order_by("topicid", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function InsertnewPost($data)
	{
		$this->db->insert('post_master',$data);
		$last_id = $this->db->insert_id();
		return $last_id;
	}
	
	public function getAllTopicPost()
	{
		$this->db->select('*');
		$this->db->where('status', 0);
		$this->db->where('xdelete', 0);
		$this->db->from('post_master');
		$this->db->order_by("topicid", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function getUserDetails12()
	{
		$this->db->select('*');
		$query=$this->db->get('usermaster');
		return $result = $query->result();
	}
	
	
}	