<?php
class Common_m extends CI_Model
{
	public function getLogoDetails()
	{
		$this->db->select('*');
		$this->db->where('status', 0);
		$this->db->where('xdelete', 0);
		$this->db->from('client_logo_details');
		$this->db->order_by("id", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function getAllTeamMembers()
	{
		$this->db->select('*');
		$this->db->where('status', 0);
		$this->db->where('xdelete', 0);
		$this->db->from('team_details');
		//$this->db->order_by("id", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function InsertQuery($data)
	{
		$this->db->insert('contact_us_master', $data);
	}
	
	public function InsertNewAppliacion($data)
	{
		$this->db->insert('usermaster', $data);
	}
	
	public function getApplicationStatus($appid,$dob)
	{
		$this->db->select('*');
		$this->db->where('application_no', $appid);
		$this->db->where('dob', $dob);
		$this->db->from('usermaster');
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	
	
	
	
}
?>