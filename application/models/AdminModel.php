<?php
class AdminModel extends CI_Model
{
	public function adminlogin($user,$pass)
	{
		$this->db->where('adminid',$user);
		$this->db->where('password',$pass);
		$this->db->where('status', 0);
		$this->db->where('xdelete', 0);
		$query=$this->db->get('adminlogin');
		return $result = $query->result();
	}
	
	public function LastLoginTimeUpdate($id,$date)
	{
		$this->db->where('slno',$id);
		$this->db->set('lastlogintime', $date);
		$this->db->update('login');
	}
	
	public function getUserId()
	{
		return $this->db->query("SELECT agentid FROM agentmaster ORDER BY slno DESC LIMIT 1")->result();
	
	}
	
	public function AgentDetailsInsert($data)
	{
		$this->db->insert('agentmaster', $data);
	}
	
	public function getAllAgentDetails()
	{
		$this->db->select('*');
		$this->db->where('xdelete', 0);
		$this->db->from('agentmaster');
		$this->db->order_by("slno", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function getAllUserDetails()
	{
		$this->db->select('*');
		$this->db->where('xdelete', 0);
		$this->db->from('usermaster');
		$this->db->order_by("slno", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function deleteThsiAgent($aid)
	{
		$this->db->where('agentid',$aid);
		$this->db->set('xdelete', 1);
		$this->db->update('agentmaster');
	}
	
	public function deleteThsiUser($uid)
	{
		$this->db->where('slno',$uid);
		$this->db->set('xdelete', 1);
		$this->db->update('usermaster');
	}
	
	public function updateThsiAgent($aid,$data)
	{
		$this->db->where('agentid',$aid);
		$this->db->update('agentmaster', $data);
	}
	
	public function updateThsiUser($uid,$data)
	{
		$this->db->where('slno',$uid);
		$this->db->update('usermaster', $data);
	}
	
	public function getAllLogoDetails()
	{
		$this->db->select('*');
		$this->db->where('xdelete', 0);
		$this->db->from('client_logo_details');
		$this->db->order_by("id", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function InsertLogoDetails($datai)
	{
		$this->db->insert('client_logo_details', $datai);
	}
	
	public function deleteThsiLogo($id)
	{
		$this->db->where('id', $id);
		$this->db->set('xdelete', 1);
		$this->db->update('client_logo_details');
	}
	
	public function updateThsiLogo($uid,$data)
	{
		$this->db->where('id', $uid);
		$this->db->update('client_logo_details', $data);
		//echo $this->db->last_query();
		//exit;
	}
	
	public function getAllTeamDetails()
	{
		$this->db->select('*');
		$this->db->where('xdelete', 0);
		$this->db->from('team_details');
		$this->db->order_by("id", "desc");
		$query = $this->db->get(); 
		return $result = $query->result();
	}
	
	public function InsertTeamDetails($datai)
	{
		$this->db->insert('team_details', $datai);
	}
	
	public function deleteThsiMember($id)
	{
		$this->db->where('id', $id);
		$this->db->set('xdelete', 1);
		$this->db->update('team_details');
	}
	
	public function updateThsiMember($uid,$data)
	{
		$this->db->where('id', $uid);
		$this->db->update('team_details', $data);
	}
	
	
	
	
}	

?>