<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LoanBondhu :: Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css')?>">
  
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/skin-blue.min.css')?>">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect

-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php include_once('header.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidemenu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1 style="padding-bottom: 15px;">Add Partner Logo</h1>
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<?php
						if($msg==1)
						{
					?>
						<div class="alert alert-success alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-check"></i> Data Sucessfully Inserted!</h4>
						</div>
					<?php
						}
					?>
					<!-- form start -->
					<form role="form" name="f1" method="post" action="<?php echo site_url('admin/insertlogo')?>"  enctype="multipart/form-data">
						<div class="col-sm-5">
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputEmail1">Partner Name</label>
									<input type="text" name="cname" class="form-control" id="exampleInputEmail1" placeholder="Enter Client Name" required>
								</div>
							</div>
							<!-- /.box-body -->
						</div>	
						<div class="col-sm-1"></div>
						<div class="col-sm-5">
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputFile1">Logo</label>
									<input type="file" name="photo" id="exampleInputFile1" accept="image/png,image/jpg,image/jpeg" required>
									<p class="help-block">Upload jpg, jepg, png image only.</p>
								</div>								
							</div>
							<!-- /.box-body -->
						</div>
						<div class="col-sm-12">			
							<div class="box-footer">
								<div class="col-sm-6" align="left">	
									<button type="reset" class="btn btn-default">Cancel</button>
								</div>
								<div class="col-sm-6" align="right">	
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
    </section>
    <!-- /.content -->
	<!-- Content Header (Page header) -->
		<section class="content-header" style="padding-top:40px;">
			<h1>
				All Partner Logo Details
				<small></small>
			</h1>      
		</section>
	<!-- Main content -->
		<section class="content container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title"></h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body" style="overflow-x: scroll;scroll-behavior: auto;">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Sl. No.</th>
										<th>Partner Name</th>
										<th>Logo</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=0;
									foreach($alllogo as $al)
									{
										$i++;
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $al->client_name; ?></td>
										<td><img src="<?php echo base_url($al->logo)?>" style="width:150px;"></td>
										<td><?php if($al->status==1) { echo "Inactive";} else if($al->status==0){ echo "Active"; } ?></td>
										<td>
											<?php
												if($al->status==1) 
												{ 
											?>
												<button onclick="activelogo('<?php echo $al->id;?>')" type="button" class="btn btn-info">Active</button>
											<?php
												} 
												else if($al->status==0)
												{ 
											?>
												<button onclick="inactivelogo('<?php echo $al->id;?>')" type="button" class="btn btn-primary">Inactive</button>
											<?php
												} 
											?>
											<button onclick="deletelogo('<?php echo $al->id;?>')" type="button" class="btn btn-danger">Delete</button>
											
										</td>
									</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
		</section>
		<!-- /.content -->
	
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php include_once('footer.php'); ?>
  
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

	<!-- jQuery 3 -->
	<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url('dist/js/adminlte.min.js')?>"></script>
	<!-- DataTables -->
	<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
	<!-- SlimScroll -->
	<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js')?>"></script>
	<script>
		$(function () {
			$('#example1').DataTable()
			$('#example2').DataTable({
				'paging'      : true,
				'lengthChange': false,
				'searching'   : false,
				'ordering'    : true,
				'info'        : true,
				'autoWidth'   : false
			})
		})
	</script>
	<script>
			function deletelogo(did)	
			{		
				var a = confirm("Do you want to Delete this Logo ?");
				if(a == true)			
				{				
					var geturl="<?php echo site_url('admin/DeleteLogo/');?>/"+did;	
					window.location.href=geturl;		
				}			
				else		
				{
				}		
			}
			function activelogo(did)	
			{		
				var a = confirm("Do you want to Active this Logo ?");
				if(a == true)			
				{				
					var geturl="<?php echo site_url('admin/ActiveLogo/');?>/"+did;	
					window.location.href=geturl;		
				}			
				else		
				{
				}		
			}	
			
			function inactivelogo(did)	
			{		
				var a = confirm("Do you want to Inactive this Logo ?");
				if(a == true)			
				{				
					var geturl="<?php echo site_url('admin/InactiveLogo/');?>/"+did;	
					window.location.href=geturl;		
				}			
				else		
				{
				}		
			}
	</script> 
</body>
</html>