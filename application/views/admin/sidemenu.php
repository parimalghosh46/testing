<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
		<!-- Sidebar Menu -->
		<ul class="sidebar-menu" data-widget="tree">
			<li>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</li>
			<!-- Optionally, you can add icons to the links -->
			<li class="active"><a href="<?php echo site_url('admin/dashboard')?>"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
			<li class="treeview">
				<a href="#"><i class="fa fa-link"></i> <span>Agent</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo site_url('admin/agentcreate')?>"><i class="fa fa-user-plus"></i>   Create</a></li>
					<li><a href="<?php echo site_url('admin/agentview')?>"><i class="fa fa-eye"></i>  View</a></li>
				</ul>
			</li>
			<li><a href="<?php echo site_url('admin/allusers')?>"><i class="fa fa-link"></i> <span>All Lead Details</span></a></li>
			<li><a href="<?php echo site_url('admin/AgentMonthlyPayout')?>"><i class="fa fa-link"></i> <span>Agent Monthly Payout</span></a></li>
			<li><a href="<?php echo site_url('admin/ContactusQuery')?>"><i class="fa fa-link"></i> <span>Contact Us Query</span></a></li>
			<li><a href="<?php echo site_url('admin/ChangePassword')?>"><i class="fa fa-link"></i> <span>Change Password</span></a></li>
			<li class="treeview">
				<a href="#"><i class="fa fa-link"></i> <span>Settings</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo site_url('admin/PartnerLogo')?>"><i class="fa fa-user-plus"></i>   Partner Logo Add</a></li>
					<li><a href="<?php echo site_url('admin/TeamAdd')?>"><i class="fa fa-eye"></i>  Team Add</a></li>
				</ul>
			</li>
		
		</ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>