<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LoanBondhu :: Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css')?>">
  
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/skin-blue.min.css')?>">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect

-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

	<!-- Main Header -->
	<?php include_once('header.php'); ?>
	<!-- Left side column. contains the logo and sidebar -->
	<?php include_once('sidemenu.php'); ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				All Lead Details
				<small></small>
			</h1>      
		</section>

		<!-- Main content -->
		<section class="content container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title"></h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body" style="overflow-x: scroll;scroll-behavior: auto;">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Sl. No.</th>
										<th>Name</th>
										<th>C/o</th>
										<th>Gender</th>
										<th>Phone</th>
										<th>Email</th>
										<th>Address</th>
										<th>Application No</th>
										<th>Applied For</th>
										<th>Added By</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=0;
									foreach($userdetails as $ud)
									{
										$i++;
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $ud->uname; ?></td>
										<td><?php echo $ud->coname; ?></td>
										<td><?php echo $ud->gender; ?></td>
										<td><?php echo $ud->phone; ?></td>
										<td><?php echo $ud->email; ?></td>
										<td><?php echo $ud->address; ?></td>
										<td><?php echo $ud->application_no; ?></td>
										<td><?php echo $ud->application_for; ?></td>
										<td><?php echo $ud->addby; ?></td>
										<td><?php if($ud->status==1) { echo "Inactive";} else if($ud->status==0){ echo "Active"; } ?></td>
										<td>
											<?php
												if($ud->status==1) 
												{ 
											?>
												<button onclick="activeuser('<?php echo $ud->slno;?>')" type="button" class="btn btn-info">Active</button>
											<?php
												} 
												else if($ud->status==0)
												{ 
											?>
												<button onclick="inactiveuser('<?php echo $ud->slno;?>')" type="button" class="btn btn-primary">Inactive</button>
											<?php
												} 
											?>
											<button onclick="activeuser('<?php echo $ud->slno;?>')" type="button" class="btn btn-danger">Delete</button>
											
										</td>
									</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<!-- Main Footer -->
	<?php include_once('footer.php'); ?>
  
</div>
<!-- ./wrapper -->

	<!-- REQUIRED JS SCRIPTS -->

	<!-- jQuery 3 -->
	<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url('dist/js/adminlte.min.js')?>"></script>
	<!-- DataTables -->
	<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
	<!-- SlimScroll -->
	<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js')?>"></script>
	<script>
		$(function () {
			$('#example1').DataTable()
			$('#example2').DataTable({
				'paging'      : true,
				'lengthChange': false,
				'searching'   : false,
				'ordering'    : true,
				'info'        : true,
				'autoWidth'   : false
			})
		})
	</script>
	<script>
			function deleteuser(did)	
			{		
				var a = confirm("Do you want to Delete this Lead ?");
				if(a == true)			
				{				
					var geturl="<?php echo site_url('admin/DeleteUser/');?>/"+did;	
					window.location.href=geturl;		
				}			
				else		
				{
				}		
			}
			function activeuser(did)	
			{		
				var a = confirm("Do you want to Active this Lead ?");
				if(a == true)			
				{				
					var geturl="<?php echo site_url('admin/ActiveUser/');?>/"+did;	
					window.location.href=geturl;		
				}			
				else		
				{
				}		
			}	
			
			function inactiveuser(did)	
			{		
				var a = confirm("Do you want to Inactive this Lead ?");
				if(a == true)			
				{				
					var geturl="<?php echo site_url('admin/InactiveUser/');?>/"+did;	
					window.location.href=geturl;		
				}			
				else		
				{
				}		
			}
	</script> 
</body>
</html>