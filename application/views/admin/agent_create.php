<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LoanBondhu :: Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/AdminLTE.min.css')?>">
  
  <link rel="stylesheet" href="<?php echo base_url('dist/css/skins/skin-blue.min.css')?>">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect

-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <?php include_once('header.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidemenu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1 style="padding-bottom: 15px;">Create Agent</h1>
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<?php
						if($msg==1)
						{
					?>
						<div class="alert alert-success alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-check"></i> Data Sucessfully Inserted!</h4>
						</div>
					<?php
						}
					?>
					<!-- form start -->
					<form role="form" name="f1" method="post" action="<?php echo site_url('admin/insertagent')?>" enctype="multipart/form-data">
						<div class="col-sm-5">
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputEmail1">Agent Name</label>
									<input type="text" name="aname" class="form-control" id="exampleInputEmail1" placeholder="Enter Agent Name" required>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Gender</label>
									<select type="text" name="gender" class="form-control" id="exampleInputgen" required>
										<option value="">Select Gender</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
										<option value="Transgender">Transgender</option>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Phone</label>
									<input type="text" name="phone" class="form-control" id="exampleInputphone" placeholder="Phone Number" required>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Email</label>
									<input type="text" name="email" class="form-control" id="exampleInputemail" placeholder="Email" required>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Status</label>
									<select type="text" name="status" class="form-control" id="exampleInputstatus" required>
										<option value="">Select Status</option>
										<option value="1">Inactive</option>
										<option value="0">Active</option>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Address</label>
									<textarea type="text" name="address" class="form-control" id="exampleInputadd" ></textarea>
								</div>
							</div>
							<!-- /.box-body -->
						</div>	
						<div class="col-sm-1"></div>
						<div class="col-sm-5">
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputPassword3">Aadhar Number</label>
									<input type="text" name="aadhar" class="form-control" id="exampleInputaadgar1" placeholder="Aadhar Number">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword2">PAN Number</label>
									<input type="text" name="pan" class="form-control" id="exampleInputaadgar2" placeholder="PAN Number">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label>
									<input type="password" name="password" class="form-control" id="exampleInputpass" placeholder="Password" required>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Re-enter Password</label>
									<input type="password" name="password1" class="form-control" id="exampleInputpass1" placeholder="Re-enter Password" required>
								</div>
								<div class="form-group">
									<label for="exampleInputFile1">Photo</label>
									<input type="file" name="photo" id="exampleInputFile1" accept="image/png,image/jpg,image/jpeg">
									<p class="help-block">Upload jpg, jepg, png image only.</p>
								</div>
								<div class="form-group">
									<label for="exampleInputFile2">Supported Document</label>
									<input type="file" name="doc" id="exampleInputFile2" accept="image/png,image/jpg,image/jpeg">
									<p class="help-block">Upload jpg, jepg, png image only.</p>
								</div>
								
							</div>
							<!-- /.box-body -->
						</div>
						<div class="col-sm-12">			
							<div class="box-footer">
								<div class="col-sm-6" align="left">	
									<button type="reset" class="btn btn-default">Cancel</button>
								</div>
								<div class="col-sm-6" align="right">	
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <?php include_once('footer.php'); ?>
  
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js')?>"></script>


</body>
</html>