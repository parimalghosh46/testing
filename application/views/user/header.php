<header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="<?php echo base_url()?>" class="scrollto">LoanBondhu</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo base_url('#intro')?>">Home</a></li>
          <li><a href="<?php echo base_url('#about')?>">About Us</a></li>
          <li><a href="<?php echo base_url('#services')?>">Services</a></li>
          <li><a href="<?php echo base_url('#portfolio')?>">Portfolio</a></li>
          <li><a href="<?php echo base_url('#team')?>">Team</a></li>
          <li><a href="<?php echo base_url('#contact')?>">Contact</a></li>
          <li><a href="<?php echo site_url('home/selectusertype');?>">Login</a></li>
		  <li class="menu-has-children"><a style="color: white;">User Name</a>
            <ul>
              <li><a href="<?php echo site_url('home/profile');?>">Profile</a></li>
              <li><a href="<?php echo site_url('home/logout');?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->