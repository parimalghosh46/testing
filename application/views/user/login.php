<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Loan Bondhu</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta content="" name="keywords">
		<meta content="" name="description">

		<!-- Favicons -->
		<link href="<?php echo base_url('img/favicon.png')?>" rel="icon">
		<link href="<?php echo base_url('img/apple-touch-icon.png')?>" rel="apple-touch-icon">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

		<!-- Bootstrap CSS File -->
		<link href="<?php echo base_url('lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

		<!-- Libraries CSS Files -->
		<link href="<?php echo base_url('lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/animate/animate.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">

		<!-- Main Stylesheet File -->
		<link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
		<style>
			#atag:hover  {
				color: #0056b3;
			}
			#atag  {
				font-weight: bold;
				font-family: sans-serif;
			}
		</style>
	  <!-- =======================================================
		Author Name: Parimal Ghosh    
	  ======================================================= -->
	</head>

	<!--body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload=""-->
	<body>

	  <!--==========================
		Header
	  ============================-->
	  <?php include_once('header.php'); ?>
	  
	  
		<section id="usertypeselect" class="section-bg wow fadeInUp" style="background: #94877e;padding: 120px 0;">
			<div class="container">
				<div class="row about-cols">

					<div class="col-md-4 wow fadeInUp" align="center">
					</div>

					<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s" align="center">
						<div class="about-col">
							<div class="img" style="text-align:center;width:30%;">
								<img src="<?php echo base_url('img/users.png')?>" alt="" class="img-fluid">
							</div>
							<h2 class="title" style="padding-top: 41px;"><a id="atag" href="<?php echo site_url('user/login') ?>">User Login</a></h2>
						</div>
					</div>
					
					<div class="col-md-4 wow fadeInUp" align="center">
					</div>
					<div class="col-md-3 wow fadeInUp" align="center">
					</div>

					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.1s" align="center">
						<?php 
						if($error==2)
						{
						?>
						<h3 style="color:red;"> ** Username or Password is blank!</h3>
						<?php
						}
						else if($error==1)
						{
						?>
						<h3 style="color:red;"> ** Username or Password Incorrect!</h3>
						<?php
						}
						?>
						<form class="form-horizontal" method="post">
							<div class="form-group">
								<div class="col-md-12">
									<input type="text" name="username" class="form-control" required placeholder="Username"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="password" name="password" class="form-control" required placeholder="Password"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<a href="#" class="btn btn-link btn-block">Forgot your password?</a>
								</div>
								<div class="col-md-6">
									<button type="submit" class="btn btn-info btn-block">Log In</button>
								</div>
							</div>
						</form>
					</div>
					
					<div class="col-md-3 wow fadeInUp" align="center">
					</div>
				</div>
			</div>
		</section><!-- #contact -->

	  </main>

	  <!--==========================
		Footer
	  ============================-->
	  <?php include_once('footer.php'); ?>
	  

		<!-- JavaScript Libraries -->
		<script src="<?php echo base_url('lib/jquery/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
		<script src="<?php echo base_url('lib/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
		<script src="<?php echo base_url('lib/easing/easing.min.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/hoverIntent.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/superfish.min.js')?>"></script>
		<script src="<?php echo base_url('lib/wow/wow.min.js')?>"></script>
		<script src="<?php echo base_url('lib/waypoints/waypoints.min.js')?>"></script>
		<script src="<?php echo base_url('lib/counterup/counterup.min.js')?>"></script>
		<script src="<?php echo base_url('lib/owlcarousel/owl.carousel.min.js')?>"></script>
		<script src="<?php echo base_url('lib/isotope/isotope.pkgd.min.js')?>"></script>
		<script src="<?php echo base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
		<script src="<?php echo base_url('lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>
		<!-- Contact Form JavaScript File -->
		<script src="<?php echo base_url('contactform/contactform.js')?>"></script>

		<!-- Template Main Javascript File -->
		<script src="<?php echo base_url('js/main.js')?>"></script>
		<!--script type="text/javascript">
			window.history.forward();
			function noBack()
			{
				window.history.forward();
			}
		</script-->
	</body>
</html>
