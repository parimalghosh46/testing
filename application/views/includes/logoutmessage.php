		<!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo site_url('Admin/Logout');?>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
		
		<div id="myModal" class="modal">

			<!-- Modal content -->
			<div class="modal-content">
				
				<span class="close">&times;</span>
				
				<div class="profile-image" align="center">
                    <img style="border: 2px solid white;border-radius: 10px;" src="<?php echo base_url($pic)?>" alt="<?php echo $name;?>"/>
					<div class="profile-data-name" style="padding-top: 15px;font-size: 20px;font-weight: bold;"><?php echo $name;?></div>
					<p style="font-size: 17px; text-align:center;"><b>Username :  <?php echo $r0->username; ?></b></p>
						<p style="font-size: 15px;"><small><b>Mobile : </b></small> <?php echo $r0->phone; ?></p>
						<p style="font-size: 15px;"><small><b>Email : </b></small> <?php echo $r0->email; ?></p>
						<p style="font-size: 15px;"><small><b>Father Name : </b></small> <?php echo $r0->fname; ?></p>
						<p style="font-size: 15px;"><small><b>Gender : </b></small> <?php echo $r0->gender; ?></p>
						<p style="font-size: 15px;"><small><b>Date Of Birth : </b></small> <?php echo $r0->dob; ?></p>
						<p style="font-size: 15px;"><small><b>Address : </b></small> <?php echo $r0->address; ?></p>
						<p style="font-size: 15px;"><small><b>User Status : </b></small> <?php if($r0->xdelete==1) echo "Inactive" ; else echo "Active"; ?></p>
					<p>
				</div>
			</div>

		</div>
		<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #e6e3e3;
    margin: auto;
    padding: 20px;
    border: 4px solid #5b5b9a;
    width: 30%;
}

/* The Close Button */
.close {
    color: red;
    float: right;
    font-size: 40px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
		
		
		
		
		
		
		
		
		
		
		
		
		<script>
			// Get the modal
			var modal = document.getElementById('myModal');

			// Get the button that opens the modal
			var btn = document.getElementById("myBtn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks the button, open the modal 
			btn.onclick = function() {
				modal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
				modal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}
		</script>