	<?php

							/*Date show start code*/

							header('Content-Type: text/html; charset=utf-8');

							/*Date show end code*/
							date_default_timezone_set('Asia/Calcutta');
							function bn_date($str)
							{
							$en = array(1,2,3,4,5,6,7,8,9,0);
							$bn = array('১','২','৩','৪','৫','৬','৭','৮','৯','০');
							$str = str_replace($en, $bn, $str);
							$en = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
							$en_short = array( 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
							$bn = array( 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'অগাস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর' );
							$str = str_replace( $en, $bn, $str );
							$str = str_replace( $en_short, $bn, $str );
							$en = array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
							$en_short = array('Sat','Sun','Mon','Tue','Wed','Thu','Fri');
							$bn_short = array('শনি', 'রবি','সোম','মঙ্গল','বুধ','বৃহঃ','শুক্র');
							$bn = array('শনিবার','রবিবার','সোমবার','মঙ্গলবার','বুধবার','বৃহস্পতিবার','শুক্রবার');
							$str = str_replace( $en, $bn, $str );
							$str = str_replace( $en_short, $bn_short, $str );
							$en = array( 'am', 'pm' );
							$bn = array( 'পূর্বাহ্ন', 'অপরাহ্ন' );
							$str = str_replace( $en, $bn, $str );
							return $str;
							}
							//Change below codes according to your requirement .
							//__________________________________________

							//echo bn_date(date('l, d M Y, h:i:s a'));
							//__________________________________________
						
		$status="";
		$name="";
		$pic ="";
		foreach($result0 as $r0)
		{
			$status = $r0->status;
			$name = $r0->name;
			$pic = $r0->profile_pic;
		}
		if($status=='admin')
		{
	?>
			<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo site_url('Admin/Adminhome')?>"><b style="color: #0024a9;">সংবাদ </b> রাতদিন </a>
						<a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?php echo base_url($pic)?>" alt="<?php echo $name;?>"/>
                        </a>
                        <div class="profile" id="myBtn">
                            <div class="profile-image">
                                <img src="<?php echo base_url($pic)?>" alt="<?php echo $name;?>"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo $name;?></div>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title" style="text-align:center; font-size:15px;">
						
						<?php echo bn_date(date('l, d M Y')); ?>
						<!--br>
						<1?php echo bn_date(date('h:i:s a')); ?-->
						
					</li>
                    <li class="active">
                        <a href="<?php echo site_url('Admin/Adminhome')?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Users</span></a>
                        <ul>
                            <li><a href="<?php echo site_url('Admin/NewUserCreate')?>"><span class="fa fa-user"></span> New User Create</a></li>
                            <li><a href="<?php echo site_url('Admin/AllUserDetails')?>"><span class="glyphicon glyphicon-user"></span> All User Details</a></li>
                        </ul>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/MenuManagemnt')?>"><span class="fa fa-indent"></span><span class="xn-text">Menu Management</span></a>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/AddNewNews')?>"><span class="fa fa-paste"></span><span class="xn-text">Add New News</span></a>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/AddUpload')?>"><span class="fa fa-plus-circle"></span><span class="xn-text">Advertisement Upload</span></a>
                    </li>
					
					<li>
                        <a href="<?php echo site_url('Admin/GalleryImageUpload')?>"><span class="fa fa-file-photo-o"></span><span class="xn-text">Gallery Image Upload</span></a>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/GalleryVideoUpload')?>"><span class="fa fa-video-camera"></span><span class="xn-text">Gallery Video Upload</span></a>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/NewsAuthentication')?>"><span class="fa fa-cogs"></span><span class="xn-text">News Edit Active Inactive</span></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('Admin/ChangePassword')?>"><span class="fa fa-warning"></span><span class="xn-text">Change Password</span></a>
                    </li>
                    
                </ul>
            </div>
			
	<?php
		}
		else
		{
	?>	
			<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo site_url('Admin/Userhome')?>"><b style="color: #0024a9;">সংবাদ </b> রাতদিন </a>
						<a href="#" class="x-navigation-control"></a>
                    </li>
					<li class="xn-profile" id="myBtn">
                        <a href="#" class="profile-mini">
                            <img src="<?php echo base_url($pic)?>" alt="<?php echo $name;?>"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?php echo base_url($pic)?>" alt="<?php echo $name;?>"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo $name;?></div>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title" style="text-align:center; font-size:15px;">
						
						<?php echo bn_date(date('l, d M Y')); ?>
						<!---br>
						<1?php echo bn_date(date('h:i:s a')); ?-->
						
					</li>
                    <li class="active">
                        <a href="<?php echo site_url('Admin/Userhome')?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li> 
					<li>
                        <a href="<?php echo site_url('Admin/AddNewNews')?>"><span class="fa fa-paste"></span><span class="xn-text">Add New News</span></a>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/AddUpload')?>"><span class="fa fa-plus-circle"></span><span class="xn-text">Advertisement Upload</span></a>
                    </li>
					
					<li>
                        <a href="<?php echo site_url('Admin/GalleryImageUpload')?>"><span class="fa fa-file-photo-o"></span><span class="xn-text">Gallery Image Upload</span></a>
                    </li>
					<li>
                        <a href="<?php echo site_url('Admin/GalleryVideoUpload')?>"><span class="fa fa-video-camera"></span><span class="xn-text">Gallery Video Upload</span></a>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR --> 
	<?php
		}
	?>
