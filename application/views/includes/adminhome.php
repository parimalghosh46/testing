<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>সংবাদ  রাতদিন - এক ক্লিকে সব খবর</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="<?php echo base_url('images/news.png')?>" type="image/png" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url('allassets/css/theme-default.css')?>"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
			<?php include_once('adminmenu.php'); ?>
            <!-- PAGE CONTENT -->
            <div class="page-content">                
                <?php include_once('adminheader.php'); ?>                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                    
                    <!-- START WIDGETS -->                    
                    <div class="row">
                        <div class="col-md-3">
                            <!-- START WIDGET SLIDER -->
                            <div class="widget widget-default widget-carousel">
                                <div class="owl-carousel" id="owl-example">
									<?php
									$newscount='';
									foreach($todaynews as $tdn)
									{
										$newscount = $tdn->count;
									}
									?>
									<a href="<?php echo site_url('Admin/NewsAuthentication');?>">
										<div>                                    
											<div class="widget-int"><?php echo $newscount;?></div>
											<div class="widget-subtitle">News Uploaded</div>
											<div class="widget-title"><?php echo bn_date(date('l, d M, Y'));?></div>
										</div>
									</a>
									<?php
									$uicimg='';
									foreach($uploadimages as $uic)
									{
										$uicimg = $uic->count;
									}
									?>
									<a href="<?php echo site_url('Admin/GalleryImageUpload');?>">
										<div> 
											<div class="widget-int"><?php echo $uicimg; ?></div>
											<div class="widget-subtitle">Images</div>
											<div class="widget-title">Total Gallery Images</div>
										</div>
									</a>
									<?php
									$uvcvdo='';
									foreach($uploadvideos as $uvc)
									{
										$uvcvdo = $uvc->count;
									}
									?>
									<a href="<?php echo site_url('Admin/GalleryVideoUpload');?>">
										<div> 
											<div class="widget-int"><?php echo $uvcvdo; ?></div>
											<div class="widget-subtitle">Videos</div>
											<div class="widget-title">Total Gallery Videos</div>
										</div>
									</a>
                                </div>                            
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                             
                            </div>         
                            <!-- END WIDGET SLIDER -->                            
                        </div>
                        <div class="col-md-3">                            
                            <!-- START WIDGET MESSAGES -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='#';">
                                <div class="widget-item-left">
                                    <span class="fa fa-envelope"></span>
                                </div> 
								<a href="<?php echo site_url('Admin/NewsAuthentication');?>">
									<div class="widget-data">
										<?php
										$newscount='';
										foreach($todaynews as $tdn)
										{
											$newscount = $tdn->count;
										}
										?>
										<div class="widget-int num-count" style="text-align: center;"><?php echo $newscount;?></div>
										<div class="widget-title" style="text-align: center;">News Uploaded</div>
										<div class="widget-subtitle" style="text-align: center;"><?php echo bn_date(date('l, d M, Y'));?></div>
									</div>
								</a>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>
                            </div>                            
                            <!-- END WIDGET MESSAGES -->                            
                        </div>
                        <div class="col-md-3">                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='#';">
                                <div class="widget-item-left">
                                    <span class="fa fa-user"></span>
                                </div>
								<a href="<?php echo site_url('Admin/AllUserDetails');?>">
									<div class="widget-data">
										<?php
										$ucont ='';
										foreach($users as $usr)
										{
											$ucont = $usr->count;
										}									
										?>
										<div class="widget-int num-count" style="text-align: center;"><?php echo $ucont;?></div>
										<div class="widget-title" style="text-align: center;">Registred users</div>
										<div class="widget-subtitle"></div>
									</div>
								</a>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->                            
                        </div>
                        <div class="col-md-3">                            
                            <!-- START WIDGET CLOCK -->
                            <div class="widget widget-info widget-padding-sm">
                                <div class="widget-big-int plugin-clock">00:00</div>                            
                                <div class="widget-subtitle plugin-date">Loading...</div>
								<!--div class="widget-big-int plugin-clock"><?php echo bn_date(date('h:i')); ?></div>                            
                                <div class="widget-subtitle plugin"><?php echo bn_date(date('l, d M, Y')); ?></div-->
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                                <div class="widget-buttons widget-c3">
                                    <div class="col">
                                        <a href="#"><span class="fa fa-clock-o"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="#"><span class="fa fa-bell"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="#"><span class="fa fa-calendar"></span></a>
                                    </div>
                                </div>                            
                            </div>                        
                            <!-- END WIDGET CLOCK -->                            
                        </div>
						<div class="col-md-3">                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='#';">
                                <div class="widget-item-left">
                                    <span class="fa fa-pencil-square-o"></span>
                                </div>
								<a href="<?php echo site_url('Admin/AllFeedback');?>">
									<div class="widget-data">
										<?php
										$fcount ='';
										foreach($feedback as $feb)
										{
											$fcount = $feb->count;
										}									
										?>
										<div class="widget-int num-count" style="text-align: center;"><?php echo $fcount;?></div>
										<div class="widget-title" style="text-align: center;">Feedback</div>
										<div class="widget-subtitle"></div>
									</div>
								</a>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->                            
                        </div>
						<div class="col-md-3">                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='#';">
                                <div class="widget-item-left">
                                    <span class="fa fa-check-circle-o"></span>
                                </div>
								<!---a href="<?php echo site_url('Admin/ShowSubscriber');?>"-->
									<div class="widget-data">
										<?php
										$subcount ='';
										foreach($subscriber as $sub)
										{
											$subcount = $sub->count;
										}									
										?>
										<div class="widget-int num-count" style="text-align: center;"><?php echo $subcount;?></div>
										<div class="widget-title" style="text-align: center;">Subscriber</div>
										<div class="widget-subtitle"></div>
									</div>
								<!--/a-->
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->                            
                        </div>
                    </div>
                    <!-- END WIDGETS -->                    
                    <!--div class="row">
                        <div class="col-md-4">                            
                            <!-- START USERS ACTIVITY BLOCK -/->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Users Activity</h3>
                                        <span>Users vs returning</span>
                                    </div>                                    
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                                            <ul class="dropdown-menu">
                                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                                            </ul>                                        
                                        </li>                                        
                                    </ul>                                    
                                </div>                                
                                <div class="panel-body padding-0">
                                    <div class="chart-holder" id="dashboard-bar-1" style="height: 200px;"></div>
                                </div>                                    
                            </div>
                            <!-- END USERS ACTIVITY BLOCK -/->                            
                        </div>
                        <div class="col-md-4">                            
                            <!-- START VISITORS BLOCK -/->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Visitors</h3>
                                        <span>Visitors (last month)</span>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                                            <ul class="dropdown-menu">
                                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                                            </ul>                                        
                                        </li>                                        
                                    </ul>
                                </div>
                                <div class="panel-body padding-0">
                                    <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;"></div>
                                </div>
                            </div>
                            <!-- END VISITORS BLOCK -/->                            
                        </div>
						<div class="col-md-4">                            
                            <!-- START PROJECTS BLOCK -/->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Projects</h3>
                                        <span>Projects activity</span>
                                    </div>                                    
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                                            <ul class="dropdown-menu">
                                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                                            </ul>                                        
                                        </li>                                        
                                    </ul>
                                </div>
                                <div class="panel-body panel-body-table">
                                    
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="50%">Project</th>
                                                    <th width="20%">Status</th>
                                                    <th width="30%">Activity</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><strong>Joli Admin</strong></td>
                                                    <td><span class="label label-danger">Developing</span></td>
                                                    <td>
                                                        <div class="progress progress-small progress-striped active">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">85%</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Gemini</strong></td>
                                                    <td><span class="label label-warning">Updating</span></td>
                                                    <td>
                                                        <div class="progress progress-small progress-striped active">
                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                                                        </div>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td><strong>Taurus</strong></td>
                                                    <td><span class="label label-warning">Updating</span></td>
                                                    <td>
                                                        <div class="progress progress-small progress-striped active">
                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 72%;">72%</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Leo</strong></td>
                                                    <td><span class="label label-success">Support</span></td>
                                                    <td>
                                                        <div class="progress progress-small progress-striped active">
                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Virgo</strong></td>
                                                    <td><span class="label label-success">Support</span></td>
                                                    <td>
                                                        <div class="progress progress-small progress-striped active">
                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                                        </div>
                                                    </td>
                                                </tr>                                                
                                            </tbody>
                                        </table>
                                    </div>                                    
                                </div>
                            </div>
                            <!-- END PROJECTS BLOCK -/->                            
                        </div>
                    </div>                    
                    <!--div class="row">
						<div class="col-md-8">                            
                            <!-- START SALES BLOCK -/->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Sales</h3>
                                        <span>Sales activity by period you selected</span>
                                    </div>                                     
                                    <ul class="panel-controls panel-controls-title">                                        
                                        <li>
                                            <div id="reportrange" class="dtrange">                                            
                                                <span></span><b class="caret"></b>
                                            </div>                                     
                                        </li>                                
                                        <li><a href="#" class="panel-fullscreen rounded"><span class="fa fa-expand"></span></a></li>
                                    </ul>                                    
                                </div>
                                <div class="panel-body">                                    
                                    <div class="row stacked">
                                        <div class="col-md-4">                                            
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong>In Queue</strong></div>
                                                <div class="pull-right">75%</div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">75%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong>Shipped Products</strong></div>
                                                <div class="pull-right">450/500</div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">90%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong class="text-danger">Returned Products</strong></div>
                                                <div class="pull-right">25/500</div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">5%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong class="text-warning">Progress Today</strong></div>
                                                <div class="pull-right">75/150</div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
                                                </div>
                                            </div>
                                            <p><span class="fa fa-warning"></span> Data update in end of each hour. You can update it manual by pressign update button</p>
                                        </div>
                                        <div class="col-md-8">
                                            <div id="dashboard-map-seles" style="width: 100%; height: 200px"></div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <!-- END SALES BLOCK -/->                            
                        </div>
						<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-content">
								<ul class="list-inline item-details">
									<li><a href="http://themifycloud.com/downloads/janux-premium-responsive-bootstrap-admin-dashboard-template/">Admin templates</a></li>
									<li><a href="http://themescloud.org">Bootstrap themes</a></li>
								</ul>
							</div>
						</div>                        
                        <div class="col-md-4">                            
                            <!-- START SALES & EVENTS BLOCK -/->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Sales & Event</h3>
                                        <span>Event "Purchase Button"</span>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                                            <ul class="dropdown-menu">
                                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                                            </ul>                                        
                                        </li>                                        
                                    </ul>
                                </div>
                                <div class="panel-body padding-0">
                                    <div class="chart-holder" id="dashboard-line-1" style="height: 200px;"></div>
                                </div>
                            </div>
                            <!-- END SALES & EVENTS BLOCK -/->                            
                        </div>
                    </div-->                    
                    <!-- START DASHBOARD CHART -->
					<div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
					<div class="block-full-width">                                                                       
                    </div>                    
                    <!-- END DASHBOARD CHART -->                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
		<?php include_once('logoutmessage.php'); ?> 
        

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?php echo base_url('allassets/audio/alert.mp3')?>" preload="auto"></audio>
        <audio id="audio-fail" src="<?php echo base_url('allassets/audio/fail.mp3')?>" preload="auto"></audio>
        <!-- END PRELOADS -->        
		<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/jquery/jquery.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/jquery/jquery-ui.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/bootstrap/bootstrap.min.js')?>"></script>       
        <!-- END PLUGINS -->
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo base_url('allassets/js/plugins/icheck/icheck.min.js')?>'></script>        
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/scrolltotop/scrolltopcontrol.js')?>"></script>        
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/morris/raphael-min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/morris/morris.min.js')?>"></script>       
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/rickshaw/d3.v3.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/rickshaw/rickshaw.min.js')?>"></script>
        <script type='text/javascript' src='<?php echo base_url('allassets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>'></script>
        <script type='text/javascript' src='<?php echo base_url('allassets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>'></script>                
        <script type='text/javascript' src='<?php echo base_url('allassets/js/plugins/bootstrap/bootstrap-datepicker.js')?>'></script>                
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/owl/owl.carousel.min.js')?>"></script>        
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/moment.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins/daterangepicker/daterangepicker.js')?>"></script>
        <!-- END THIS PAGE PLUGINS-->       

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url('allassets/js/settings.js')?>"></script>        
        <script type="text/javascript" src="<?php echo base_url('allassets/js/plugins.js')?>"></script>        
        <script type="text/javascript" src="<?php echo base_url('allassets/js/actions.js')?>"></script>        
        <script type="text/javascript" src="<?php echo base_url('allassets/js/demo_dashboard.js')?>"></script>
        <!-- END TEMPLATE -->
		<!-- END SCRIPTS -->         
    </body>
</html>