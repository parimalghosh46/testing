<footer id="footer">
    <div class="footer_top">
		<div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single_footer_top wow fadeInLeft">
				<h2>Information</h2>
				<ul class="catg3_snav ppost_nav">
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="<?php echo site_url('Home/AboutUs');?>"> About Us</a></div>
					</li>
					<!--li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="#"> Blog</a></div>
					</li-->
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="<?php echo site_url('Home/TermsConditions');?>"> Terms & Conditions</a></div>
					</li>
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="<?php echo site_url('Home/PrivacyPolicy');?>"> Privacy Policy</a></div>
					</li>
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="<?php echo site_url('Home/ContactUs');?>"> Contact Us</a></div>
					</li>
				</ul>
			</div>
		</div>
        <div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single_footer_top wow fadeInLeft">
				<h2>Followed By Email</h2>
				<div class="subscribe_area">
					<p>"Subscribe here to get our newsletter, it is safe just Put your Email and click subscribe"</p>
					<form action="<?php echo site_url('Home/Subscribe')?>" method="POST" >
						<div class="subscribe_mail">
						<input class="form-control" type="email" name="subemail" placeholder="Email Address">
						<i class="fa fa-envelope"></i></div>
						<input class="submit_btn" type="submit" value="Submit">
					</form>
				</div>
			</div>
		</div>
		<!--div class="col-lg-3 col-md-3 col-sm-6">
			<div class="single_footer_top wow fadeInRight">
				<h2>Links</h2>
				<ul class="catg3_snav ppost_nav">
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="#"> Blog Home</a></div>
					</li>
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="#"> Blog</a></div>
					</li>
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="#"> Error Page</a></div>
					</li>
					<li style="padding-top: 0px;">
						<div class="media-body"> <a class="catg_title" href="#"> Wpfreeware</a></div>
					</li>
				</ul>
			</div>
		</div-->
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="single_footer_top wow fadeInRight">
				<h2>Feedback</h2>
				<form name="f1" onsubmit="return validateForm()" action="<?php echo site_url('Home/Feedback')?>" class="contact_form" method="POST">
					<div class="col-md-6">
						<label>Name</label>
						<input class="form-control" type="text" name="name" id="name" >
					</div>
					<div class="col-md-6">
						<label>Email*</label>
						<input class="form-control" type="email" name="email" id="email">
					</div>
					<div class="col-md-12">
						<label>Message*</label>
						<textarea class="form-control" name="message" id="message"></textarea>
					</div>
					<div class="col-md-12">
						<input class="send_btn" type="submit" value="Send">
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="footer_bottom">
		<div class="footer_bottom_left">
			<p>Copyright &copy; 2018</p>
		</div>
		<div class="footer_bottom_right">
			<ul>
				<!--li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Twitter" href="#"><i class="fa fa-twitter"></i></a></li-->
				<li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Facebook" target="_blank" href="https://www.facebook.com/sangbadraatdin/"><i class="fa fa-facebook"></i></a></li>
				<li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Googel+" target="_blank" href="https://plus.google.com/103873156399502360576"><i class="fa fa-google-plus"></i></a></li>
				<li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Youtube" target="_blank" href="https://www.youtube.com/channel/UCw2agVVsjJzK0p5KwOqXCxg"><i class="fa fa-youtube"></i></a></li>
				<!--li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Rss" href="#"><i class="fa fa-rss"></i></a></li-->
			</ul>
		</div>
	</div>
</footer>
<script>
	function validateForm() {
		var x = document.forms["f1"]["name"].value;
		var y = document.forms["f1"]["email"].value;
		var z = document.forms["f1"]["message"].value;
		if (x == "")
		{
			$("#name").css("border", "1px solid red");
			return false;
		}
		else if(y == "")
		{
			$("#name").css("border", "none");
			$("#email").css("border", "1px solid red");
			return false;
		}
		else if(z == "")
		{
			$("#name").css("border", "none");
			$("#email").css("border", "none");
			$("#message").css("border", "1px solid red");
			return false;
		}
		else
		{
			return true;
		}
	}
</script>