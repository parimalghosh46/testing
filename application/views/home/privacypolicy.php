<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Loan Bondhu</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta content="" name="keywords">
		<meta content="" name="description">

		<!-- Favicons -->
		<link href="<?php echo base_url('img/favicon.png')?>" rel="icon">
		<link href="<?php echo base_url('img/apple-touch-icon.png')?>" rel="apple-touch-icon">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

		<!-- Bootstrap CSS File -->
		<link href="<?php echo base_url('lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

		<!-- Libraries CSS Files -->
		<link href="<?php echo base_url('lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/animate/animate.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">

		<!-- Main Stylesheet File -->
		<link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
		<style>
			#atag:hover  {
				color: #0056b3;
			}
			#atag  {
				font-weight: bold;
				font-family: sans-serif;
			}
		</style>
	  <!-- =======================================================
		Author Name: Parimal Ghosh    
	  ======================================================= -->
	</head>

	<!--body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload=""-->
	<body>

	  <!--==========================
		Header
	  ============================-->
	  <?php include_once('header.php'); ?>
	  
	  
		<section id="usertypeselect" class="section-bg wow fadeInUp" style="background: url(<?php echo base_url('img/about-bg.jpg')?>) center top no-repeat fixed;padding: 120px 0;">
			<div class="container">
				<div class="row about-cols">

					<div class="col-md-12 wow fadeInUp" align="center">
						<h2 style="Color:black;font-weight:bold;">Privacy Policy</h2>
						
						<div class="col-md-1"></div>
						<div class="col-md-10" style="background-color: #ffffffd9;color:black;padding: 20px;">
							<p style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Loanbondhu.com</b> (`Website`) recognizes the importance of maintaining your privacy. Loanbondhu.com is committed to maintain the confidentiality, integrity and security of all information of our users. This Privacy Policy describes how Loanbondhu.com collects and handles certain information it may collect and/or receive from you via the use of this Website. Please see below for details on what type of information we may collect from you, how that information is used in connection with the services offered through our Website and other shared with our business partners. This Privacy Policy applies to current and former visitors to our Website and to our online customers. By visiting and/or using our website, you agree to this Privacy Policy.</p>
							<p style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;By using the Loanbondhu.com and/or registering yourself at www.Loanbondhu.com you authorize Loanbondhu Marketing and Consulting Private Limited and Etechaces Marketing and Consulting Private Limited (including it`s representatives, affiliates, and its business partners) to contact you via email or phone call or sms and offer you our services for the product you have opted for, imparting product knowledge, offer promotional offers running on Loanbondhu.com and offers by its business partners and associated third parties, for which reasons your information may be collected in the manner as detailed under this Policy. You hereby agree that you authorize Loanbondhu.com to contact you for the above mentioned purposes even if you have registered yourself under DND or DNC or NCPR service(s). Your authorization, in this regard, shall be valid as long as your account is not deactivated by either you or us.</p>
							<div  style="text-align: left;">
								<h3>Controllers of Personal Information</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your personal data will be stored and collected by Loanbondhu Marketing and Consulting Private Limited along with its parent company Etechaces Marketing and Consulting Private Limited which wholly owns Loanbondhu.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Purposes of collection of your data</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loanbondhu.com collects your information when you register for an account, when you use its products or services, visit its Website's pages, and when you enter promotions or sweepstakes as featured on the Website. When you register with Loanbondhu.com, you are asked for your first name, last name, state and city of residence, email address, date of birth, and sex. Once you register at the Website and sign in you are not anonymous to us. Also, you are asked for your contact number during registration and may be sent SMSs, notifications about our services to your wireless device. Hence, by registering you authorize the Loanbondhu.com to send texts and email alerts to you with your login details and any other service requirements, including promotional mails and SMSs.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>We use your information in order to:</h3>
								<p style="text-align: left;">
								<br>•	respond to queries or requests submitted by you.
								<br>•	process orders or applications submitted by you.
								<br>•	administer or otherwise carry out our obligations in relation to any agreement with our business partners.
								<br>•	anticipate and resolve problems with any services supplied to you.
								<br>•	to send you information about special promotions or offers. We might also tell you about new features or products. These might be our own offers or products, or third-party offers or products with whom Loanbondhu.com has a tie-up.
								<br>•	to make our website and the services offered by Loanbondhu.com better. We may combine information we get from you with information about you we get from third parties.
								<br>•	to send you notices, communications, offer alerts relevant to your use of the Services offered on this Website.
								<br>•	as otherwise provided in this Privacy Policy.

								Some features of this Website or our Services will require you to furnish your personally identifiable information as provided by you under your account section on our Website.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Information Sharing and Disclosure</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loanbondhu.com will not sell or rent your information to anyone other than as set forth in this Privacy Policy. Notwithstanding the foregoing, we may share your information to an affiliate and/or business partner. Loanbondhu.com may also share, sell, and/or transfer your personally identifiable information to any successor-in-interest as a result of a sale of any part of Loanbondhu.com's business or upon the merger, reorganization, or consolidation of it with another entity on a basis that it is not the surviving entity. For the purposes of this paragraph, "affiliate" means any person, company, or Loanbondhu.com directly, or indirectly through one or more intermediaries, that controls, is controlled by or is under common control with Loanbondhu.com (The term "control," as used in the preceding sentence, shall mean with respect to any person, the possession, directly or indirectly, of the power, through the exercise of voting rights, contractual rights or otherwise, to direct or cause the direction of the management or policies of the controlled person. As used in this Privacy Policy, the term "person" includes any natural person, corporation, partnership, limited liability company, trust, unincorporated association, or any other entity).
								</p>
							</div>
							<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We limit the collection and use of your personal information. We may make anonymous or aggregate personal information and disclose such data only in a non-personally identifiable manner. Such information does not identify you individually. Access to your Account information and any other personal identifiably information is strictly restricted and used only in accordance with specific internal procedures, in order to operate, develop or improve our Services. We may use third party service providers to enable you to provide with Our services and we require such third parties to maintain the confidentiality of the information we provide to them under our contracts with them.
							</p>
							<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We may also share your information, without obtaining your prior written consent, with government agencies mandated under the law to obtain information for the purpose of verification of identity, or for prevention, detection, investigation including cyber incidents, prosecution, and punishment of offences, or where disclosure is necessary for compliance of a legal obligation. Any Information may be required to be disclosed to any third party by us by an order under the law for the time being in force.
							</p>
							<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There are number of products/services such as loans, credit cards, mutual funds, offered by third Parties on the Website, such as lenders, banks, credit card issuers. If you choose to apply for these separate products or services, disclose information to these providers, then their use of your information is governed by their privacy policies. Loanbondhu.com is not responsible for their privacy policies.
							</p>
							<div style="text-align: left;">
								<h3>We Collect Cookies</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A cookie is a piece of data stored on the user's computer tied to information about the user. We may use both session ID cookies and persistent cookies. For session ID cookies, once you close your browser or log out, the cookie terminates and is erased. A persistent cookie is a small text file stored on your computer?s hard drive for an extended period of time. Session ID cookies may be used by PRP to track user preferences while the user is visiting the website. They also help to minimize load times and save on server processing. Persistent cookies may be used by PRP to store whether, for example, you want your password remembered or not, and other information. Cookies used on the PRP website do not contain personally identifiable information.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Log Files</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Like most standard websites, we use log files. This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, platform type, date/time stamp, and number of clicks to analyze trends, administer the site, track user's movement in the aggregate, and gather broad demographic information for aggregate use. We may combine this automatically collected log information with other information we collect about you. We do this to improve services we offer to you, to improve marketing, analytics or site functionality.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Email- Opt out</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If you are no longer interested in receiving e-mail announcements and other marketing information from us, please e-mail your request at: info@Loanbondhu.com. Please note that it may take about 10 days to process your request.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Security</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We employ appropriate technical and organizational security measures at all times to protect the information we collect from you. We use multiple electronic, procedural, and physical security measures to protect against unauthorized or unlawful use or alteration of information, and against any accidental loss, destruction, or damage to information. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, we cannot guarantee its absolute security. Further, you are responsible for maintaining the confidentiality and security of your login id and password, and may not provide these credentials to any third party.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Third Party Advertising</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We may use third-party advertising companies and/or ad agencies to serve ads when you visit our Website. These companies may use information (excluding your name, address, email address, or telephone number) about your visits to this Website in order to provide advertisements on this Website and other third party websites about goods and services that may be of interest to you.
								</p>
							</div>
							<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We use third-party service providers to serve ads on our behalf across the internet and sometimes on this Website. They may collect anonymous information about your visits to Website, and your interaction with our products and services. They may also use information about your visits to this and other Websites for targeted advertisements for goods and services. This anonymous information is collected through the use of a pixel tag, which is industry standard technology used by most major Websites. No personally identifiable information is collected or used in this process.
							</p>
							<div style="text-align: left;">
								<h3>Links to Other Websites</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There might be affiliates or other sites linked to Loanbondhu.com. Personal information that you provide to those sites are not our property. These affiliated sites may have different privacy practices and we encourage you to read their privacy policies of these website when you visit them.
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Changes in this Privacy Policy</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loanbondhu.com reserves the right to change this policy from time to time, at its sole discretion. We may update this privacy policy to reflect changes to our information practices. We encourage you to periodically review
								</p>
							</div>
							<div style="text-align: left;">
								<h3>Grievance Manager</h3>
								<p style="text-align: left;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In case you have any grievances with respect to in accordance with applicable law on Information Technology and rules made there under, the name and contact details of the Grievance Officer are provided below:
								<br>
								Mr. Anamitra Jana
								</p>
							</div>
							<div style="text-align: left;">
								<h3><b>Loanbondhu.com</b></h3>
								<p style="text-align: left;">
								Mobile - +91 6376014340
								<br>
								Email : info@loanbondhu.com
								<br>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If you have questions, concerns, or suggestions regarding our Privacy Policy, we can be reached using the contact information on our Contact Us page or at info@loanbondhu.com.
								</p>
							</div>
							
						
						</div>
						
					</div>
				</div>
			</div>
		</section><!-- #contact -->

	  </main>

	  <!--==========================
		Footer
	  ============================-->
	  <?php include_once('footer.php'); ?>
	  

		<!-- JavaScript Libraries -->
		<script src="<?php echo base_url('lib/jquery/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
		<script src="<?php echo base_url('lib/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
		<script src="<?php echo base_url('lib/easing/easing.min.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/hoverIntent.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/superfish.min.js')?>"></script>
		<script src="<?php echo base_url('lib/wow/wow.min.js')?>"></script>
		<script src="<?php echo base_url('lib/waypoints/waypoints.min.js')?>"></script>
		<script src="<?php echo base_url('lib/counterup/counterup.min.js')?>"></script>
		<script src="<?php echo base_url('lib/owlcarousel/owl.carousel.min.js')?>"></script>
		<script src="<?php echo base_url('lib/isotope/isotope.pkgd.min.js')?>"></script>
		<script src="<?php echo base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
		<script src="<?php echo base_url('lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>
		<!-- Contact Form JavaScript File -->
		<script src="<?php echo base_url('contactform/contactform.js')?>"></script>

		<!-- Template Main Javascript File -->
		<script src="<?php echo base_url('js/main.js')?>"></script>
		<!--script type="text/javascript">
			window.history.forward();
			function noBack()
			{
				window.history.forward();
			}
		</script-->
	</body>
</html>
