<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Loan Bondhu</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta content="" name="keywords">
		<meta content="" name="description">

		<!-- Favicons -->
		<link href="<?php echo base_url('img/favicon.png')?>" rel="icon">
		<link href="<?php echo base_url('img/apple-touch-icon.png')?>" rel="apple-touch-icon">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

		<!-- Bootstrap CSS File -->
		<link href="<?php echo base_url('lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

		<!-- Libraries CSS Files -->
		<link href="<?php echo base_url('lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/animate/animate.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">

		<!-- Main Stylesheet File -->
		<link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
		<style>
			#atag:hover  {
				color: #0056b3;
			}
			#atag  {
				font-weight: bold;
				font-family: sans-serif;
			}
		</style>
	  <!-- =======================================================
		Author Name: Parimal Ghosh    
	  ======================================================= -->
	</head>

	<!--body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload=""-->
	<body>

	  <!--==========================
		Header
	  ============================-->
	  <?php include_once('header.php'); ?>
	  
	  
		<section id="usertypeselect" class="section-bg wow fadeInUp" style="background: url(<?php echo base_url('img/about-bg.jpg')?>) center top no-repeat fixed;padding: 120px 0;">
			<div class="container">
				<div class="row about-cols">

					<div class="col-md-12 wow fadeInUp" align="center">
						<h2 style="color:black;font-weight:bold;">Terms & Conditions</h2>
						<div class="col-md-1"></div>
						<div class="col-md-10" style="background-color: #ffffffd9;color:black;padding: 20px;">
						<div  style="text-align: left;">
							<h3>Proprietary rights</h3>
							<p style="text-align: left;">You acknowledge and agree that Loanbondhu owns all rights, title and interest in and to the Services, including any intellectual property rights which subsist in the Services (whether registered or not). You further acknowledge that the Services may contain information which is designated confidential by Loanbondhu and that you shall not disclose such information without Loanbondhu's prior written consent</p>
							<p style="text-align: left;">Loanbondhu grants you a limited license to access and make personal use of the Website and the Services. This license does not confer any right to download, copy, create a derivative work from, modify, reverse engineer, reverse assemble or otherwise attempt to discover any source code, sell, assign, sub-license, grant a security interest in or otherwise transfer any right in the Services. You do not have the right to use any of Loanbondhu's trade names, trademarks, service marks, logos, domain names, and other distinctive brand features. You do not have the right to remove, obscure, or alter any proprietary rights notices (including trademark and copyright notices), which may be affixed to or contained within the Services. You will not copy or transmit any of the Services.</p>
						</div>
						<div  style="text-align: left;">
							<h3>Usage of the Website</h3>
							<p style="text-align: left;">The Website is intended for personal and non-commercial use. Only register to become a member of the Website if you are above the age of 18 and can enter into binding contracts. You are responsible for maintaining the secrecy of your passwords, login and account information. You will be responsible for all use of the Website by you and anyone using your password and login information (with or without your permission).</p>
							<p style="text-align: left;">You also agree to provide true, accurate, current and complete information about yourself as prompted by the Website. If you provide any information that is untrue, inaccurate, not updated or incomplete (or becomes untrue, inaccurate, not updated or incomplete), or Loanbondhu has reasonable grounds to suspect that such information is untrue, inaccurate, not updated or incomplete, Loanbondhu has the right to suspend or terminate your account and refuse any and all current or future use of the Website (or any portion thereof) or Services in connection thereto.</p>
							<p style="text-align: left;">By making use of the Website, and furnishing your personal/contact details, you hereby agree that you are interested in availing and purchasing the Services that you have selected. You hereby agree that Loanbondhu may contact you either electronically or through phone, to understand your interest in the selected products and Services and to fulfil your demand or complete your application. You specifically understand and agree that by using the Website you authorize Loanbondhu and its affiliates and partners to contact you for any follow up calls in relation to the Services provided through the Website. You expressly waive the Do Not Call registrations on your phone/mobile numbers for contacting you for this purpose. You also agree that Loanbondhu reserves the right to make your details available to partners and you may be contacted by the partners for information and for sales through email, telephone and/or sms. You agree to receive promotional materials and/or special offers from Loanbondhu through emails or sms.</p>
							<p style="text-align: left;">The usage of the Website may also require you to provide consent for keying in your Sensitive Information (“SI”) (including but not limited to user ids and passwords), as may be necessary to process your application through this Website. SI keyed in shall be required for enabling hassle free, faster and paperless (to the extent possible) processing of applications for financial products so opted / availed by you. Loanbondhu shall adhere to best industry practices including information security, data protection and privacy law while processing such applications and the SI interface where key in of SI is required shall not be stored. However, Loanbondhu shall not be liable to you against any liability or claims which may arise out of such transactions.</p>
							<p style="text-align: left;">You may only use the Website to search for and to apply for loans, mutual funds, credit cards or other financial products as may be displayed on the Website from time to time and you shall not use the Website to make any fraudulent applications. You agree not to use the Website for any purpose that is unlawful, illegal or forbidden by these Terms, or any local laws that might apply to you. Since the Website is in operation in India, while using the Website, you shall agree to comply with laws that apply to India and your own country (in case of you being a foreign national). We may, at our sole discretion, at any time and without advance notice or liability, suspend, terminate or restrict your access to all or any component of the Website.</p>
							<p style="text-align: left;">You are prohibited from posting or transmitting to or through this Website: (i) any unlawful, threatening, libelous, defamatory, obscene, pornographic, or other material or content that would violate rights of publicity and/or privacy or that would violate any law; (ii) any commercial material or content (including, but not limited to, solicitation of funds, advertising, or marketing of any good or services); and (iii) any material or content that infringes, misappropriates or violates any copyright, trademark, patent right or other proprietary right of any third party. You shall be solely liable for any damages resulting from any violation of the foregoing restrictions, or any other harm resulting from your posting of content to this Website.</p>
						</div>
						<div  style="text-align: left;">
							<h3>Our partners</h3>
							<p style="text-align: left;">Display of loan, mutual funds, credit card or other financial products, offered by third parties, on the Website does not in any way imply, suggest, or constitute any sponsorship, recommendation, opinion, advice or approval of Loanbondhu against such third parties or their products. You agree that Loanbondhu is in no way responsible for the accuracy, timeliness or completeness of information it may obtain from these third parties. Your interaction with any third party accessed through the Website is at your own risk, and Loanbondhu will have no liability with respect to the acts, omissions, errors, representations, warranties, breaches or negligence of any such third parties or for any personal injuries, death, property damage, or other damages or expenses resulting from your interactions with the third parties.</p>
							<p style="text-align: left;">You agree and acknowledge that the credit shall be at the sole discretion of Loanbondhu’s financial partners (lenders, credit card companies, mutual fund companies) while making any application through the Website for a financial product offered by such financial partners; Loanbondhu shall not be held liable for any delay, rejection or approval of any application made through its Website.</p>
						</div>
						<div  style="text-align: left;">
							<h3>Disclaimer of warranty</h3>
							<p style="text-align: left;">The Website and all content and Services provided on the Website are provided on an "as is" and "as available" basis. Loanbondhu expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, title, non-infringement, and security and accuracy, as well as all warranties arising by usage of trade, course of dealing, or course of performance. Loanbondhu makes no warranty, and expressly disclaims any obligation, that: (a) the content will be up-to-date, complete, comprehensive, accurate or applicable to your circumstances; (b) The Website will meet your requirements or will be available on an uninterrupted, timely, secure, or error-free basis; (c) the results that may be obtained from the use of the Website or any Services offered through the Website will be accurate or reliable; or (d) the quality of any products, services, information, or other material obtained by you through the Website will meet your expectations.</p>
						</div>
						<div  style="text-align: left;">
							<h3>Limitation of liability</h3>
							<p style="text-align: left;">Loanbondhu (including its officers, directors, employees, representatives, affiliates, and providers) will not be responsible or liable for (a) any injury, death, loss, claim, act of god, accident, delay, or any direct, special, exemplary, punitive, indirect, incidental or consequential damages of any kind (including without limitation lost profits or lost savings), whether based in contract, tort, strict liability or otherwise, that arise out of or is in any way connected with (i) any failure or delay (including without limitation the use of or inability to use any component of the Website), or (ii) any use of the Website or content, or (iii) the performance or non-performance by us or any provider, even if we have been advised of the possibility of damages to such parties or any other party, or (b) any damages to or viruses that may infect your computer equipment or other property as the result of your access to the Website or your downloading of any content from the Website.</p>
							<p style="text-align: left;">The Website may provide links to other third party websites. However, since Loanbondhu has no control over such third party websites, you acknowledge and agree that Loanbondhu is not responsible for the availability of such third party websites, and does not endorse and is not responsible or liable for any content, advertising, products or other materials on or available from such third party websites. You further acknowledge and agree that Loanbondhu shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party websites.</p>
						</div>
						<div  style="text-align: left;">
							<h3>Indemnity</h3>
							<p style="text-align: left;">You agree to indemnify and hold Loanbondhu (and its officers, directors, agents and employees) harmless from any and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature, including reasonable attorneys' fees, or arising out of or related to your breach of this Terms, your violation of any law or the rights of a third party, or your use of the Website.</p>
						</div>
						<div  style="text-align: left;">
							<h3>Additional terms</h3>
							<p style="text-align: left;">You may not assign or otherwise transfer your rights or obligations under these Terms. Loanbondhu may assign its rights and duties under these Terms without any such assignment being considered a change to the Terms and without any notice to you. If we fail to act on your breach or anyone else's breach on any occasion, we are not waiving our right to act with respect to future or similar breaches. Other terms and conditions may apply to loans, mutual funds, credit cards or other financial products that you may apply on the Website. You will observe these other terms and conditions. The laws of the India, without regard to its conflict of laws rules, will govern these Terms, as well as your and our observance of the same. If you take any legal action relating to your use of the Website or these Terms, you agree to file such action only in the courts located in Chennai, India. In any such action that we may initiate, the prevailing party will be entitled to recover all legal expenses incurred in connection with the legal action, including but not limited to costs, both taxable and non-taxable, and reasonable attorney fees. You acknowledge that you have read and have understood these Terms, and that these Terms have the same force and effect as a signed agreement.</p>
						</div>
					</div>
				</div>
			</div>
		</section><!-- #contact -->

	  </main>

	  <!--==========================
		Footer
	  ============================-->
	  <?php include_once('footer.php'); ?>
	  

		<!-- JavaScript Libraries -->
		<script src="<?php echo base_url('lib/jquery/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
		<script src="<?php echo base_url('lib/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
		<script src="<?php echo base_url('lib/easing/easing.min.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/hoverIntent.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/superfish.min.js')?>"></script>
		<script src="<?php echo base_url('lib/wow/wow.min.js')?>"></script>
		<script src="<?php echo base_url('lib/waypoints/waypoints.min.js')?>"></script>
		<script src="<?php echo base_url('lib/counterup/counterup.min.js')?>"></script>
		<script src="<?php echo base_url('lib/owlcarousel/owl.carousel.min.js')?>"></script>
		<script src="<?php echo base_url('lib/isotope/isotope.pkgd.min.js')?>"></script>
		<script src="<?php echo base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
		<script src="<?php echo base_url('lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>
		<!-- Contact Form JavaScript File -->
		<script src="<?php echo base_url('contactform/contactform.js')?>"></script>

		<!-- Template Main Javascript File -->
		<script src="<?php echo base_url('js/main.js')?>"></script>
		<!--script type="text/javascript">
			window.history.forward();
			function noBack()
			{
				window.history.forward();
			}
		</script-->
	</body>
</html>
