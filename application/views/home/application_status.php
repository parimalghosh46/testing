<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Loan Bondhu</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta content="" name="keywords">
		<meta content="" name="description">

		<!-- Favicons -->
		<link href="<?php echo base_url('img/favicon.png')?>" rel="icon">
		<link href="<?php echo base_url('img/apple-touch-icon.png')?>" rel="apple-touch-icon">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

		<!-- Bootstrap CSS File -->
		<link href="<?php echo base_url('lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

		<!-- Libraries CSS Files -->
		<link href="<?php echo base_url('lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/animate/animate.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">

		<!-- Main Stylesheet File -->
		<link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
		
		<!------ Include the above in your HEAD tag ---------->
		<style>
			body{
				background: -webkit-linear-gradient(left, #0072ff, #00c6ff);
			}
			.contact-form{
				background: #fff;
				margin-top: 3%;
				margin-bottom: 5%;
				width: 90%;
			}
			.contact-form .form-control{
				border-radius:1rem;
			}
			.contact-image{
				text-align: center;
			}
			.contact-image img{
				border-radius: 6rem;
				width: 11%;
				margin-top: -3%;
				transform: rotate(29deg);
			}
			.contact-form form{
				padding: 14% 4%;
			}
			.contact-form form .row{
				margin-bottom: -7%;
			}
			.contact-form h3{
				margin-bottom: 8%;
				margin-top: -10%;
				text-align: center;
				color: #0062cc;
			}
			.contact-form .btnContact {
				width: 50%;
				border: none;
				border-radius: 1rem;
				padding: 1.5%;
				background: #dc3545;
				font-weight: 600;
				color: #fff;
				cursor: pointer;
			}
			.btnContactSubmit
			{
				width: 50%;
				border-radius: 1rem;
				padding: 1.5%;
				color: #fff;
				background-color: #0062cc;
				border: none;
				cursor: pointer;
			}
			@media (min-width: 1200px){
				.container {
					max-width: 100%;
				}
			}
		</style>
		
	</head>
	<body>
		<!--==========================
		Header
		============================-->
		<?php include_once('header.php'); ?>
		<section id="usertypeselect" class="section-bg wow fadeInUp" style="background: -webkit-linear-gradient(left, #3931af, #00c6ff);padding: 120px 0;">
			<div class="container contact-form">
				<div class="contact-image">
					<img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
				</div>
				<form method="post" id="idForm">
					<h3>Check Your Application Status</h3>
				   <div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<input type="text" name="application_no" class="form-control" placeholder="Your Application Number *" required/>
							</div>
							<div class="form-group">
								<input type="date" name="dob" class="form-control" placeholder="Your Date of Birth *" required/>
							</div>
							<div class="form-group">
								<input type="submit" name="btnSubmit" class="btnContact" value="Check" />
							</div>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-5" id="statusview">
							<?php
							if($status != '')
							{
								if($view == 0)
								{
							?>
							<div style="border: 2px solid black;padding: 20px;border-radius: 10px;">
							<div class="form-group">
								<label style="font-size: 25px;"><b>Application Number : </b> <span><?php echo $status[0]->application_no; ?></span></label> 
							</div>
							<div class="form-group">
								<label style="font-size: 25px;"><b>Name : </b><span><?php echo $status[0]->uname ?></span></label> 
							</div>
							<div class="form-group">
								<label style="font-size: 25px;"><b>Status : </b><span><?php echo $status[0]->status ?></span></label> 
							</div>
							</div>
							<?php
								}
							}
							if($view == 1)
							{
							?>	
							<div style="border: 2px solid black;padding: 20px;border-radius: 10px;">
								<h2 style="color:red;">Incorrect Input!</h2>
							</div>
							<?php
							}
							if($view == 2)
							{
								
							}
							?>
						</div>
					</div>
				</form>
			</div>
		</section>
		
		<!--==========================
		Footer
		============================-->
		<?php include_once('footer.php'); ?>
		
		<script src="<?php echo base_url('lib/jquery/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
		<script src="<?php echo base_url('lib/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
		<script src="<?php echo base_url('lib/easing/easing.min.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/hoverIntent.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/superfish.min.js')?>"></script>
		<script src="<?php echo base_url('lib/wow/wow.min.js')?>"></script>
		<script src="<?php echo base_url('lib/waypoints/waypoints.min.js')?>"></script>
		<script src="<?php echo base_url('lib/counterup/counterup.min.js')?>"></script>
		<script src="<?php echo base_url('lib/owlcarousel/owl.carousel.min.js')?>"></script>
		<script src="<?php echo base_url('lib/isotope/isotope.pkgd.min.js')?>"></script>
		<script src="<?php echo base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
		<script src="<?php echo base_url('lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>
		<!-- Contact Form JavaScript File -->
		<script src="<?php echo base_url('contactform/contactform.js')?>"></script>

		<!-- Template Main Javascript File -->
		<script src="<?php echo base_url('js/main.js')?>"></script>
		
		<!--script>
			$("#idForm").on("submit", function (e) {
				e.preventDefault();
				var url = "<?php echo site_url('home/checkAppStatus'); ?>";

				$.ajax({
					url : url,
					type: "POST",
					data: $(this).serialize(),
					dataType: "JSON",
					success: function()
					{
						alert('error');
					},
					error: function(data)
					{
						alert(data);
						$('#statusview').html(data)
						//alert('Successfully!! Added Purchase Order');
						//window.location.href = "<?php echo base_url(); ?>purchaseorder/view";
					}
				});
			});			
		</script-->
	</body>
	
</html>