<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Loan Bondhu</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url('img/favicon.png')?>" rel="icon">
  <link href="<?php echo base_url('img/apple-touch-icon.png')?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url('lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url('lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('lib/animate/animate.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('lib/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
	<style>
		#clients img {
			padding: 15px 10px;
			height: 120px;
			width: 250px;
		}
		.img-fluid {
			height: 200px;
		}
	</style>
  <!-- =======================================================
    Author Name: Parimal Ghosh    
  ======================================================= -->
</head>

<body>

	<!--==========================
			Header
	============================-->
	<?php include_once('header.php'); ?>

	<!--==========================
			Intro Section
	============================-->
	<section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="carousel-background"><img src="<?php echo base_url('uploads/background/banner_1.jpg')?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Loan Bondhu : Your Credit Partner</h2>
                <p>To become a world-class finance company and online market place offering a spectrum of banking products and financial services to retail and corporate customers through a variety of delivery channels.</p>
                <!--a href="#featured-services" class="btn-get-started scrollto">Get Started</a-->
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="<?php echo base_url('uploads/background/banner_2.jpg')?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Loan Bondhu : Your Credit Partner</h2>
                <p>To become a world-class finance company and online market place offering a spectrum of banking products and financial services to retail and corporate customers through a variety of delivery channels.</p>
                <!--a href="#featured-services" class="btn-get-started scrollto">Get Started</a-->
              </div>
            </div>
          </div>

          

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
    
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>About Us</h3>
          <p>LoanBondhu is a dynamic company, with a youthful and enthusiastic team determined to accomplish the vision of becoming a world-class finance company. We offers a spectrum of banking products and financial services to retail and corporate customers through a variety of delivery channels. We has been able to create those kinds of lasting relationships with each of our clients through loyalty and trust. We have continued to revert back to this idea, which has made us one of the largest financial leaders in India. A steady growth rate validates the trust that the industry has confided in LoanBondhu as a strategic business partner and a financial solution provider.</p>
        </header>

        <div class="row about-cols">

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="<?php echo base_url('uploads/background/our_mission.jpg')?>" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Mission</a></h2>
              <p>To become a world-class finance company and online market place offering a spectrum of banking products and financial services to retail and corporate customers through a variety of delivery channels.</p>

				<p>To help borrowers secure loans (cheaper and faster) and help lenders make better credit risk decisions.</p>

				<p>To create a credit platform which is operated using cutting edge IT tools & Technologies.</p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <div class="img">
                <img src="<?php echo base_url('uploads/background/our _plan.jpg')?>" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-list-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Plan</a></h2>
              <p>To become a world-class finance company and online market place offering a spectrum of banking products and financial services to retail and corporate customers through a variety of delivery channels.</p>

				<p>To help borrowers secure loans (cheaper and faster) and help lenders make better credit risk decisions.</p>

				<p>To create a credit platform which is operated using cutting edge IT tools & Technologies.</p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
				<div class="img">
					<img src="<?php echo base_url('uploads/background/our_vision.jpg')?>" alt="" class="img-fluid">
					<div class="icon"><i class="ion-ios-eye-outline"></i></div>
				</div>
				<h2 class="title"><a href="#">Our Vision</a></h2>
				<p>To become a world-class finance company and online market place offering a spectrum of banking products and financial services to retail and corporate customers through a variety of delivery channels.</p>

				<p>To help borrowers secure loans (cheaper and faster) and help lenders make better credit risk decisions.</p>

				<p>To create a credit platform which is operated using cutting edge IT tools & Technologies.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="specialities">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>Specialities</h3>
          <!--p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus, ad pro quaestio laboramus. Ei ubique vivendum pro. At ius nisl accusam lorenta zanos paradigno tridexa panatarel.</p-->
        </header>

        <div class="row">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-home-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Home Loan</a></h4>
            <p class="description">A home equity loan is a type of loan in which the borrower uses the equity of his or her home as collateral. The loan amount is determined by the value of the property, and the value of the property is determined by an appraiser from the lending institution.[1] Home equity loans are often used to finance major expenses such as home repairs, medical bills, or college education. A home equity loan creates a lien against the borrower's house and reduces actual home equity.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-bookmarks-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Personal Loan</a></h4>
            <p class="description">A personal loan is a type of unsecured loan and helps you meet your current financial needs. You don't usually need to pledge any security or collateral while availing a personal loan and your lender provides you with the flexibility to use the funds as per your need</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-paper-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Business Loan</a></h4>
            <p class="description">A business loan is a loan specifically intended for business purposes. As with all loans, it involves the creation of a debt, which will be repaid with added interest</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-speedometer-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Vehicle Loan</a></h4>
            <p class="description">The subject of car finance comprises the different financial products which allows someone to acquire a car with any arrangement other than a single lump payment. The provision of car finance by a third party supplier allows the acquirer to provide for and raise the funds to compensate the initial owner, either a dealer or manufacturer.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-barcode-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Loan Against Poperty</a></h4>
            <p class="description">A loan against property (LAP) is exactly what the name implies -- a loan given or disbursed against the mortgage of property. The loan is given as a certain percentage of the property's market value, usually around 40 per cent to 60 per cent</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">NCDs/ Bonds</a></h4>
            <p class="description">NCDs/BONDS. Non Convertible Debentures (NCDs) or Bonds are debt investments in which an investor lends money to an entity (corporation or governmental) that borrows the funds for a specified period of time at a fixed interest rate.</p>
          </div>
		  <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Mutual Fund</a></h4>
            <p class="description">A mutual fund is a professionally managed investment fund that pools money from many investors to purchase securities. These investors may be retail or institutional in nature. Mutual funds have advantages and disadvantages compared to direct investing in individual securities.</p>
          </div>
		  <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Fixed Deposit</a></h4>
            <p class="description">A fixed deposit is a financial instrument provided by banks or NBFCs which provides investors a higher rate of interest than a regular savings account, until the given maturity date. It may or may not require the creation of a separate account.</p>
          </div>
		  <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline" style="font-size: 50px;"></i></div>
            <h4 class="title"><a href="#">Insurance</a></h4>
            <p class="description">Insurance is a means of protection from financial loss. It is a form of risk management, primarily used to hedge against the risk of a contingent or uncertain loss. An entity which provides insurance is known as an insurer, insurance company, insurance carrier or underwriter.</p>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <!--section id="call-to-action" class="wow fadeIn">
      <div class="container text-center">
        <h3>Call To Action</h3>
        <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <a class="cta-btn" href="#">Call To Action</a>
      </div>
    </section--><!-- #call-to-action -->

    <!--==========================
      Skills Section
    ============================-->
    <!--section id="skills">
      <div class="container">

        <header class="section-header">
          <h3>Our Skills</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        </header>

        <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">90%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">55%</i></span>
            </div>
          </div>

        </div>

      </div>
    </section-->

    <!--==========================
      Facts Section
    ============================-->
    <!--section id="facts"  class="wow fadeIn">
      <div class="container">

        <header class="section-header">
          <h3>Facts</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </header>

        <div class="row counters">

  				<div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">274</span>
            <p>Clients</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">421</span>
            <p>Projects</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,364</span>
            <p>Hours Of Support</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>Hard Workers</p>
  				</div>

  			</div>

        <div class="facts-img">
          <img src="<?php echo base_url('img/facts-img.png')?>" alt="" class="img-fluid">
        </div>

      </div>
    </section--><!-- #facts -->

    <!--==========================
      Portfolio Section
    ============================-->
    <!--section id="portfolio"  class="section-bg" >
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Our Portfolio</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/app1.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/app1.jpg')?>" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">App 1</a></h4>
                <p>App</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/web3.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/web3.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">Web 3</a></h4>
                <p>Web</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/app2.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/app2.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">App 2</a></h4>
                <p>App</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/card2.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/card2.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">Card 2</a></h4>
                <p>Card</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/web2.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/web2.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">Web 2</a></h4>
                <p>Web</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/app3.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/app3.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">App 3</a></h4>
                <p>App</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/card1.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/card1.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">Card 1</a></h4>
                <p>Card</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/card3.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/card3.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">Card 3</a></h4>
                <p>Card</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <figure>
                <img src="<?php echo base_url('img/portfolio/web1.jpg')?>" class="img-fluid" alt="">
                <a href="<?php echo base_url('img/portfolio/web1.jpg')?>" class="link-preview" data-lightbox="portfolio" data-title="Web 1" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="#">Web 1</a></h4>
                <p>Web</p>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section--><!-- #portfolio -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Our Partners</h3>
        </header>

        <div class="owl-carousel clients-carousel">
		<?php 
		foreach($alllogo as $al)
		{
		?>
          <img src="<?php echo base_url($al->logo)?>" alt="<?php echo $al->client_name;?>">
        <?php
		}
		?>
        </div>

      </div>
    </section><!-- #clients -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="owl-carousel testimonials-carousel">
			
			<div class="testimonial-item">
				<img src="<?php echo base_url('uploads/testimonials/parimal.jpg')?>" class="testimonial-img" alt="">
				<h3>Parimal Ghosh</h3>
				<h4>Designer & Developer</h4>
				<p>
					<img src="<?php echo base_url('img/quote-sign-left.png')?>" class="quote-sign-left" alt="">
						Finance is not merely about making money. It's about achieving our deep goals and protecting the fruits of our labor. It's about stewardship and, therefore, about achieving the good society.
					<img src="<?php echo base_url('img/quote-sign-right.png')?>" class="quote-sign-right" alt="">
				</p>
			</div>
			

			<div class="testimonial-item">
				<img src="<?php echo base_url('uploads/testimonials/soumya.jpg')?>" class="testimonial-img" alt="">
				<h3>Soumya Kanti Jana</h3>
				<h4>Designer</h4>
				<p>
					<img src="<?php echo base_url('img/quote-sign-left.png')?>" class="quote-sign-left" alt="">
					In almost every profession - whether it's law or journalism, finance or medicine or academia or running a small business - people rely on confidential communications to do their jobs. We count on the space of trust that confidentiality provides. When someone breaches that trust, we are all worse off for it. 
					<img src="<?php echo base_url('img/quote-sign-right.png')?>" class="quote-sign-right" alt="">
				</p>
			</div>
        </div>

      </div>
    </section><!-- #testimonials -->

    <!--==========================
      Team Section
    ============================-->
    <section id="team">
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3>Team</h3>
          <p>A team is a group of individuals working together to achieve a goal</p>
        </div>

        <div class="row">
			<?php
			foreach($teammembers as $tm)
			{
			?>
			<div class="col-lg-4 col-md-6 wow fadeInUp">
				<div class="member">
					<?php
					if($tm->photo!='')
					{
					?>
					<img src="<?php echo base_url($tm->photo)?>" class="img-fluid" alt="">
					<?php
					}
					else
					{
					?>
					<img src="<?php echo base_url('img/demo.jpg')?>" class="img-fluid" alt="">
					<?php					
					}
					?>
					
					<div class="member-info">
						<div class="member-info-content">
							<h4><?php echo $tm->member_name; ?></h4>
							<span><?php echo $tm->position; ?></span>
							<div class="social">
								<?php
								if($tm->twitter!='' || $tm->twitter!=0)
								{
								?>
								<a target="_blank" href="<?php echo $tm->twitter ?>"><i class="fa fa-twitter"></i></a>
								<?php
								}
								if($tm->facebook!='' || $tm->facebook!=0)
								{
								?>
								<a target="_blank" href="<?php echo $tm->facebook ?>"><i class="fa fa-facebook"></i></a>
								<?php
								}
								if($tm->google_plus!='' || $tm->google_plus!=0)
								{
								?>
								<a target="_blank" href="<?php echo $tm->google_plus ?>"><i class="fa fa-google-plus"></i></a>
								<?php
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
			?>
        </div>

      </div>
    </section><!-- #team -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>Contact Us</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-6">
            <div class="contact-phone" style="border-left:none;">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+155895548855">+91 6376014340</a></p>
            </div>
          </div>

          <div class="col-md-6">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:info@loanbondhu.com">info@loanbondhu.com</a></p>
            </div>
          </div>

        </div>

        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
			<div id="errormessage"></div>
			<form action="<?php echo site_url('home/sendmessage');?>" method="post" role="form" class="contactForm1">
				<div class="form-row">
					<div class="form-group col-md-6">
						<input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required/>
						<div class="validation"></div>
					</div>
					<div class="form-group col-md-6">
						<input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" required />
						<div class="validation"></div>
					</div>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" required/>
					<div class="validation"></div>
				</div>
				<div class="form-group">
					<textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message" required></textarea>
					<div class="validation"></div>
				</div>
				<div class="text-center"><button type="submit" name="submit">Send Message</button></div>
			</form>
        </div>

      </div>
    </section><!-- #contact -->

  </main>

	<!--==========================
			Footer
	============================-->
	<?php include_once('footer.php'); ?>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url('lib/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
  <script src="<?php echo base_url('lib/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
  <script src="<?php echo base_url('lib/easing/easing.min.js')?>"></script>
  <script src="<?php echo base_url('lib/superfish/hoverIntent.js')?>"></script>
  <script src="<?php echo base_url('lib/superfish/superfish.min.js')?>"></script>
  <script src="<?php echo base_url('lib/wow/wow.min.js')?>"></script>
  <script src="<?php echo base_url('lib/waypoints/waypoints.min.js')?>"></script>
  <script src="<?php echo base_url('lib/counterup/counterup.min.js')?>"></script>
  <script src="<?php echo base_url('lib/owlcarousel/owl.carousel.min.js')?>"></script>
  <script src="<?php echo base_url('lib/isotope/isotope.pkgd.min.js')?>"></script>
  <script src="<?php echo base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
  <script src="<?php echo base_url('lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url('contactform/contactform.js')?>"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url('js/main.js')?>"></script>

</body>
</html>
