<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>LoanBondhu</h3>
            <!--p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p-->
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?php echo base_url('#intro')?>">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?php echo base_url('#about')?>">About us</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?php echo base_url('#specialities')?>">Specialities</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?php echo site_url('home/TermsandConditions')?>">Terms and Conditions</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?php echo site_url('home/PrivacyPolicy')?>">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contact Us</h4>         
              <strong>Phone:</strong> +91 6376014340<br>
              <strong>Email:</strong> info@loanbondhu.com<br>
            </p>

            <div class="social-links">
              <a href="https://twitter.com/LBondhu" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/loanbondhu/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/loanbondhu/" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="https://plus.google.com/u/1/102774564738352188913" target="_blank" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="https://www.linkedin.com/in/loan-bondhu-998818161" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <!--div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>If you have not been able to get a lot of money, then you will be able to pay a higher price for the first time as well as the most expensive caramase for the first time in the next year.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div-->

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>A Madhumita Enterprise Initiative</strong>.
      </div>
      <div class="credits">
        
        Designed by <a href="https://bootstrapmade.com/">Parimal Ghosh</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>