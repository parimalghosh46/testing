<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Loan Bondhu</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta content="" name="keywords">
		<meta content="" name="description">

		<!-- Favicons -->
		<link href="<?php echo base_url('img/favicon.png')?>" rel="icon">
		<link href="<?php echo base_url('img/apple-touch-icon.png')?>" rel="apple-touch-icon">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

		<!-- Bootstrap CSS File -->
		<link href="<?php echo base_url('lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

		<!-- Libraries CSS Files -->
		<link href="<?php echo base_url('lib/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/animate/animate.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/ionicons/css/ionicons.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('lib/lightbox/css/lightbox.min.css')?>" rel="stylesheet">

		<!-- Main Stylesheet File -->
		<link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
		<style>
			#atag:hover  {
				color: #0056b3;
			}
			#atag  {
				font-weight: bold;
				font-family: sans-serif;
			}
			.register{
				background: -webkit-linear-gradient(left, #3931af, #00c6ff);
				margin-top: 3%;
				/*padding: 3%;*/
			}
			.register-left{
				text-align: center;
				color: #fff;
				margin-top: 4%;
			}
			.register-left input{
				border: none;
				border-radius: 1.5rem;
				padding: 2%;
				width: 100%;
				background: #f8f9fa;
				font-weight: bold;
				color: #383d41;
				margin-top: 30%;
				margin-bottom: 3%;
				cursor: pointer;
			}
			.register-right{
				background: #f8f9fa;
				border-top-left-radius: 10% 50%;
				border-bottom-left-radius: 10% 50%;
				width: 99.5%;
			}
			.register-left img{
				margin-top: 15%;
				margin-bottom: 5%;
				width: 25%;
				-webkit-animation: mover 2s infinite  alternate;
				animation: mover 1s infinite  alternate;
			}
			@-webkit-keyframes mover {
				0% { transform: translateY(0); }
				100% { transform: translateY(-20px); }
			}
			@keyframes mover {
				0% { transform: translateY(0); }
				100% { transform: translateY(-20px); }
			}
			.register-left p{
				font-weight: lighter;
				padding: 12%;
				margin-top: -9%;
			}
			.register .register-form{
				padding: 10%;
				margin-top: 10%;
			}
			.btnRegister{
				float: right;
				margin-top: 10%;
				border: none;
				border-radius: 1.5rem;
				padding: 2%;
				background: #0062cc;
				color: #fff;
				font-weight: 600;
				width: 50%;
				cursor: pointer;
			}
			.register .nav-tabs{
				margin-top: 3%;
				border: none;
				background: #0062cc;
				border-radius: 1.5rem;
				width: 28%;
				float: right;
			}
			.register .nav-tabs .nav-link{
				padding: 2%;
				height: 34px;
				font-weight: 600;
				color: #fff;
				border-top-right-radius: 1.5rem;
				border-bottom-right-radius: 1.5rem;
			}
			.register .nav-tabs .nav-link:hover{
				border: none;
			}
			.register .nav-tabs .nav-link.active{
				width: 100px;
				color: #0062cc;
				border: 2px solid #0062cc;
				border-top-left-radius: 1.5rem;
				border-bottom-left-radius: 1.5rem;
			}
			.register-heading{
				text-align: center;
				margin-top: 8%;
				margin-bottom: -15%;
				color: #495057;
			}
			
			@media (min-width: 1200px)
			{
				.container {
					max-width: 98%;
				}
			}
		</style>
	  <!-- =======================================================
		Author Name: Parimal Ghosh    
	  ======================================================= -->
	</head>

	<!--body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload=""-->
	<body>

	  <!--==========================
		Header
	  ============================-->
	  <?php include_once('header.php'); ?>
	  
	  
		<section id="usertypeselect" class="section-bg wow fadeInUp" style="background: -webkit-linear-gradient(left, #3931af, #00c6ff);padding: 120px 0;">
			<div class="container register" style="background:none;">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <p>You are 30 seconds away from earning your own money!</p>
                        <a href="<?php echo site_url('home/CheckApplicationStatus')?>"><input type="submit" name="" value="Check Application Status"/></a><br/>
                    </div>
                    <div class="col-md-9 register-right">
                        <!--ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Employee</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Hirer</a>
                            </li>
                        </ul-->
                        <div class="tab-content" id="myTabContent">
							<form name="f1" method="post" action="<?php echo site_url('home/userapplication')?>">
								<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
									<h3 class="register-heading">New Application</h3>
									<?php
									//if($this->session->flashdata('msg'))
									//{/
									?>
									<h3 class="register-heading" style="color:#155724;"><?php echo $this->session->flashdata('msg')?></h3>
									<?php
									//}
									?>
									<div class="row register-form">
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="fname" class="form-control" placeholder="First Name *" value="" required/>
											</div>
											<div class="form-group">
												<input type="text" name="lname" class="form-control" placeholder="Last Name *" value="" required/>
											</div>
											<div class="form-group">
												<input type="text" name="coname" class="form-control"  placeholder="C/O *" value="" required/>
											</div>
											<div class="form-group">
												<input type="date" name="dob" class="form-control" placeholder="Date of Bitrh *" required/>
											</div>
											
											<div class="form-group">
												<div class="maxl">
													<label class="radio inline"> 
														<input type="radio" name="gender" value="male" checked style="width: 30px;height: 20px;">
														<span> Male </span> 
													</label>
													<label class="radio inline"> 
														<input type="radio" name="gender" value="female" style="width: 30px;height: 20px;">
														<span>Female </span> 
													</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="email" name="email" id="email" class="form-control" placeholder="Your Email *" value="" required/>
											</div>
											<div class="form-group">
												<input type="text" minlength="10" maxlength="10" name="phone" id="phone" class="form-control" placeholder="Your Phone *" value="" required/>
											</div>
											<div class="form-group">
												<textarea name="address" id="address" type="text" class="form-control" placeholder="Enter Address" style="height:95px;"></textarea>
											</div>
											<div class="form-group">
												<select type="text" name="application_for" class="form-control" required>
													<option value="">Select One</option>
													<option value="Home Loan">Home Loan</option>
													<option value="Personal Loan">Personal Loan</option>
													<option value="Business Loan">Business Loan</option>
													<option value="Vehicle Loan">Vehicle Loan</option>
													<option value="Loan Against Poperty">Loan Against Poperty</option>
													<option value="NCDs/ Bonds">NCDs/ Bonds</option>
													<option value="Mutual Fund">Mutual Fund</option>
													<option value="Fixed Deposit">Fixed Deposit</option>
													<option value="Insurance">Insurance</option>
												</select>
											</div>
											<input type="submit" class="btnRegister"  value="Register"/>
										</div>
									</div>
								</div>
							</form>
                            <!--div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <h3  class="register-heading">Apply as a Hirer</h3>
                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="First Name *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Last Name *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" maxlength="10" minlength="10" class="form-control" placeholder="Phone *" value="" />
                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Password *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Confirm Password *" value="" />
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option class="hidden"  selected disabled>Please select your Sequrity Question</option>
                                                <option>What is your Birthdate?</option>
                                                <option>What is Your old Phone Number</option>
                                                <option>What is your Pet Name?</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="`Answer *" value="" />
                                        </div>
                                        <input type="submit" class="btnRegister"  value="Register"/>
                                    </div>
                                </div>
                            </div-->
                        </div>
                    </div>
                </div>
			</div>
		</section><!-- #contact -->

	  </main>

	  <!--==========================
		Footer
	  ============================-->
	  <?php include_once('footer.php'); ?>
	  

		<!-- JavaScript Libraries -->
		<script src="<?php echo base_url('lib/jquery/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
		<script src="<?php echo base_url('lib/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
		<script src="<?php echo base_url('lib/easing/easing.min.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/hoverIntent.js')?>"></script>
		<script src="<?php echo base_url('lib/superfish/superfish.min.js')?>"></script>
		<script src="<?php echo base_url('lib/wow/wow.min.js')?>"></script>
		<script src="<?php echo base_url('lib/waypoints/waypoints.min.js')?>"></script>
		<script src="<?php echo base_url('lib/counterup/counterup.min.js')?>"></script>
		<script src="<?php echo base_url('lib/owlcarousel/owl.carousel.min.js')?>"></script>
		<script src="<?php echo base_url('lib/isotope/isotope.pkgd.min.js')?>"></script>
		<script src="<?php echo base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
		<script src="<?php echo base_url('lib/touchSwipe/jquery.touchSwipe.min.js')?>"></script>
		<!-- Contact Form JavaScript File -->
		<script src="<?php echo base_url('contactform/contactform.js')?>"></script>

		<!-- Template Main Javascript File -->
		<script src="<?php echo base_url('js/main.js')?>"></script>
		<!--script type="text/javascript">
			window.history.forward();
			function noBack()
			{
				window.history.forward();
			}
		</script-->
	</body>
</html>
