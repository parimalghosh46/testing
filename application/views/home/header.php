<header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a style="border:none;" href="<?php echo base_url()?>"><img style="margin-top: -10px;" src="<?php echo base_url('img/loan_logo.png') ?>"></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>
		<!--?php 
			//$url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 
			$url = $_SERVER['SERVER_NAME']; 
			
			$dataforcheck = "localhost/MyPanel/admin/CheckMypanel/".$url;
			
			//print_r($dataforcheck);
			
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$dataforcheck);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
			if (empty($buffer)){
				print "Nothing returned from url.<p>";
			}
			else{
				print_r($buffer);
			}
		
		?-->
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo base_url('#intro')?>">Home</a></li>
          <li><a href="<?php echo base_url('#about')?>">About Us</a></li>
          <li><a href="<?php echo base_url('#specialities')?>">Specialities</a></li>
          <!--li><a href="<?php echo base_url('#portfolio')?>">Portfolio</a></li-->
          <li><a href="<?php echo base_url('#team')?>">Team</a></li>
          <li><a href="<?php echo base_url('#contact')?>">Contact</a></li>
          <!--li><a href="<?php echo site_url('home/selectusertype');?>">Login</a></li-->
		  <li class="menu-has-children"><a style="color: white;">Apply Now</a>
            <ul>
              <li><a href="<?php echo site_url('home/NewApplication');?>">New Application</a></li>
              <li><a href="<?php echo site_url('home/CheckApplicationStatus');?>">Check Application Status</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->