            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="#">User</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <!--li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?php echo base_url('allassets/assets/images/users/avatar.jpg')?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?php echo base_url('allassets/assets/images/users/avatar.jpg')?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">John Doe</div>
                                <div class="profile-data-title">Web Developer/Designer</div>
                            </div>
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li-->
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<?php echo site_url('User/Userhome')?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li> 
					<li>
                        <a href="<?php echo site_url('User/AddNewNews')?>"><span class="fa fa-file"></span><span class="xn-text">Add New News</span></a>
                    </li> 
					<li>
                        <a href="<?php echo site_url('User/ChangePassword')?>"><span class="fa fa-file"></span><span class="xn-text">Change Password</span></a>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR --> 