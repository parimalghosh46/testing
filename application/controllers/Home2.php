<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* Title */
		$data['titleactive']=$this->ProjectmainModel->getTitleActiveNewsHome($date,$time);
		$data['title']=$this->ProjectmainModel->getTitleNewsHome($date,$time);
		/* Daylong */
		$data['daylong']=$this->ProjectmainModel->getDaylongNewsHome($date,$time);		
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Entertainment */
		$data['ftentertainment']=$this->ProjectmainModel->getFirstThreeEntertainment($date,$time);
		$data['stentertainment']=$this->ProjectmainModel->getSecondThreeEntertainment($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date,$time);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/* Special Stories */
		$data['specialstories']=$this->ProjectmainModel->getThreeSpecialStories($date,$time);
		/* Blog */
		$data['blog']=$this->ProjectmainModel->getBlogHome();
		/* Image Gallery */
		$data['galleryimage']=$this->ProjectmainModel->getGalleryImageLimit();
		/* Video Gallery */
		$data['galleryvideo']=$this->ProjectmainModel->getGalleryVideoLimit();
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		
		$this->load->view('homepage',$data);
	}
	
	public function SingleNews($nid)
	{
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$result = $this->ProjectmainModel->getclickCountById($nid);
		if(!$result)
		{
			$data1 = array(
				'newsid' => $nid,
				'clickcount' => 1,
				'xdelete' => 0,
			);
			$this->ProjectmainModel->InsertCountClick($data1);
		}
		else
		{
			$click = '';
			foreach($result as $r)
			{
				$click = $r->clickcount;
			}
			$clickcnt = $click + 1;
			$data2 = array(
					'clickcount' => $clickcnt,
					'xdelete' => 0,
			);
			$this->ProjectmainModel->UpdateCountClick($data2,$nid);
		}	
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Single News */
		$data['singlenews']=$this->ProjectmainModel->getSinglenewsbyId($nid);
		$data['todatnews']=$this->ProjectmainModel->getAllTodayNews($date,$time);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('single_news_page',$data);
	}
	
	/*  শিরোনাম */
	public function Title()
	{
		$data['title'] = "শিরোনাম";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['headerthree']=$this->ProjectmainModel->getThreeTitle($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsitle($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*  দিনভর */
	public function TodayAllNews()
	{
		$data['title'] = "দিনভর";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['todatnews']=$this->ProjectmainModel->getAllTodayNews($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('today_all_news',$data);
	}
	
	/*    কলকাতা   */
	public function Kolkata() 
	{
		$data['title'] = "কলকাতা";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*Kolkata Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeKolkata($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsKolkata($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);		
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*     রাজ্য      */
	public function State() 
	{
		$data['title'] = "রাজ্য";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeState($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsState($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*    দেশ      */
	public function National() 
	{
		$data['title'] = "দেশ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeNational($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsNational($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      বিদেশ      */
	public function International() 
	{
		$data['title'] = "বিদেশ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeInternational($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsInternational($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      বাণিজ্য        */
	public function Business() 
	{
		$data['title'] = "বাণিজ্য";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeBusiness($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsBusiness($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*        বিনোদন        */
	public function Entertainment() 
	{
		$data['title'] = "বিনোদন";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeEntertainment($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsEntertainment($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*        খেলা       */
	public function Sports() 
	{
		$data['title'] = "খেলা";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeSports($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsSports($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*        উত্তরবঙ্গ      */
	public function Northbengal() 
	{
		$data['title'] = "উত্তরবঙ্গ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeNorthbengal($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsNorthbengal($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      লাইফস্টাইল      */
	public function Lifestyle() 
	{
		$data['title'] = "লাইফস্টাইল";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeLifestyle($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsLifestyle($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      গল্প নয়, সত্যি      */
	public function Offbeat() 
	{
		$data['title'] = "অফবিট";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeOffbeat($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsOffbeat($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      সফর      */
	public function Tour() 
	{
		$data['title'] = "সফর";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeTour($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsTour($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      সম্পাদকীয়      */
	public function Editorial() 
	{
		$data['title'] = "সম্পাদকীয়";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeEditorial($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsEditorial($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      বিজ্ঞান প্রযুক্তি      */
	public function Sciencetechnology() 
	{
		$data['title'] = " বিজ্ঞান প্রযুক্তি ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeSciencetechnology($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsSciencetechnology($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*      শিক্ষা স্বাস্থ্য      */
	public function Helth() 
	{
		$data['title'] = "শিক্ষা স্বাস্থ্য ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/*State Home*/
		$data['headerthree']=$this->ProjectmainModel->getThreeHelth($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllNewsHelth($date,$time);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*     স্পেশাল স্টোরিজ      */
	public function SpecialStories() 
	{
		$data['title'] = "স্পেশাল স্টোরিজ ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['headerthree']=$this->ProjectmainModel->getThreeSpecialStories($date,$time);
		$data['allnews']=$this->ProjectmainModel->getAllSpecialStories($date,$time);
		
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('category_single_page',$data);
	}
	
	/*     ব্লগ    */
	public function Blog() 
	{
		$data['title'] = "ব্লগ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['allblog']=$this->ProjectmainModel->getAllBlog();
		
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('blog_all',$data);
	}
	
	/*     ব্লগ    */
	public function SingleBlog($nid) 
	{
		$data['title'] = "ব্লগ";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['sblog'] = $nid;
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['singleblog']=$this->ProjectmainModel->getSingleBlog($nid);
		$data['todatnews']=$this->ProjectmainModel->getAllTodayNews($date,$time);
		$data['allblog']=$this->ProjectmainModel->getAllBlog();
		
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('blog_single',$data);
	}
	
	public function ShowAllVideos()
	{
		$data['title'] = "ভিডিও গ্যালারী";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['allvideos']=$this->ProjectmainModel->getAllGalleryVideos();
		/* Breaking */
		
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('video_gallery',$data);
	}
	
	public function ShowSingleVideos($vid)
	{
		$data['title'] = "ভিডিও গ্যালারী";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['videoid'] = $vid;
		$data['menu'] = $this->ProjectmainModel->getAllActiveMenu();
		$data['allvideos'] = $this->ProjectmainModel->getAllGalleryVideos();
		$data['allvideossingle'] = $this->ProjectmainModel->getGalleryVideosById($vid);
		/* Breaking */
		
		
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('video_gallery_single',$data);
	}
	
	public function ShowAllImages()
	{
		$data['title'] = "ছবির গ্যালারী";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* All Images */
		$data['allgalleryimage']=$this->ProjectmainModel->getAllGalleryImages();
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('image_gallery',$data);
	}
	
	public function ShowSingleImage($id)
	{
		$data['title'] = "ছবির গ্যালারী";
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		$data['imageid'] = $id;
		$data['allgalleryimage']=$this->ProjectmainModel->getAllGalleryImages();
		$data['galleryimage']=$this->ProjectmainModel->getGalleryImagesById($id);
		/*Kolkata*/
		$data['kolkata']=$this->ProjectmainModel->getKolkataNewsHome($date,$time);
		$data['kolkatabanner']=$this->ProjectmainModel->getKolkataNewsHomeBanner($date,$time);
		/*State*/
		$data['state']=$this->ProjectmainModel->getStateNewsHome($date,$time);
		$data['statebanner']=$this->ProjectmainModel->getStateNewsHomeBanner($date,$time);
		/*National*/
		$data['national']=$this->ProjectmainModel->getNationalNewsHome($date,$time);
		$data['nationalbanner']=$this->ProjectmainModel->getNationalNewsHomeBanner($date,$time);
		/*International*/
		$data['international']=$this->ProjectmainModel->getInternationalNewsHome($date,$time);
		$data['internationalbanner']=$this->ProjectmainModel->getInternationalNewsHomeBanner($date,$time);
		/* Propuler News */
		$data['populernews']=$this->ProjectmainModel->getPopulerNews($date);
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('image_gallery_single',$data);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public function AboutUs()
	{
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('aboutus',$data);	
	}
	
	public function PrivacyPolicy()
	{
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('privacypolicy',$data);	
	}
	
	public function TermsConditions()
	{
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('termsconditions',$data);	
	}
	
	public function ContactUs()
	{
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('contactus',$data);	
	}
	
	
	
	public function Blankpage()
	{
		$date = date('Y-m-d');
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$data['menu']=$this->ProjectmainModel->getAllActiveMenu();
		/* Breaking News */
		$data['breaking']=$this->ProjectmainModel->getBreakingNews($date,$time);
		/*Advertisement*/
		$data['advertisement']=$this->ProjectmainModel->getAllAdvertisement();
		$this->load->view('blank_page',$data);		
	}
	
	
	
	
	
	
	
	
	public function clickCount()
	{
		$this->load->model('ProjectmainModel');
		$search_data = $_POST['search_data'] ;
		$result = $this->ProjectmainModel->getclickCountById($search_data);
		if(!$result)
		{
			$data = array(
				'newsid' => $search_data,
				'clickcount' => 1,
				'xdelete' => 0,
			);
			$this->ProjectmainModel->InsertCountClick($data);
		}
		else
		{
			$click = '';
			foreach($result as $r)
			{
				$click = $r->clickcount;
			}
			$clickcnt = $click + 1;
			$data = array(
					'clickcount' => $clickcnt,
					'xdelete' => 0,
			);
			$this->ProjectmainModel->UpdateCountClick($data,$search_data);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function Feedback()
	{
		$this->load->model('ProjectmainModel');
		$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'message' => $this->input->post('message'),
				'xdelete' => 0,
		);
		$this->ProjectmainModel->InsertFeedback($data);	
		echo "<script type=\"text/javascript\">alert('Thank You For Your Feedback');window.location.href='index';</script>";
	}
	
	public function Subscribe()
	{
		$this->load->model('ProjectmainModel');
		$data = array(
				'email' => $this->input->post('subemail'),
		);
		$this->ProjectmainModel->InsertSubscribe($data);
		echo "<script type=\"text/javascript\">alert('Thank You For Subscribe Our Page ');window.location.href='index';</script>";
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */