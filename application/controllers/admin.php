<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//date_default_timezone_set('Asia/Calcutta');
class Admin extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->library('session');
        $this->load->helper(array('form', 'url'));
		$this->load->model('AdminModel');
    }
	
	public function index()
	{
		if($_POST)
		{
			$user=$this->input->post('adminid');
			$pass=$this->input->post('password');
			if($user !='' && $pass !='')
			{
				$result=$this->AdminModel->adminlogin($user,$pass);
				$id="";
				$name="";
				$date="";
				$status="";
				if(COUNT($result)>0)
				{
					foreach ($result as $r)
					{
						$id=$r->adminid;
						$name=$r->name;
						//$date=$r->lastlogintime;
						$status=$r->status;
					}
				
					$data= array(
						'slno'=> $id,
						'username'=> $name,
						'lastlogintime'=>$date,
						'status'=>$status,
					);
					$datetime = date('Y-m-d h:i:s a', time());
					$this->session->set_userdata($data);
					$userid = $this->session->userdata('slno');
					$username = $this->session->userdata('username');
					$status = $this->session->userdata('status');
					$dt= $this->session->userdata('lastlogintime');
					//$this->AdminModel->LastLoginTimeUpdate($id,$datetime);
					redirect("admin/dashboard");
				}
				else
				{
					$data['error'] = 1; //Userid & Password Incorrect
					$this->load->view('admin/login',$data);
				}
			}
			else
			{
				$data['error'] = 2; //Userid & Password Blank
				$this->load->view('admin/login',$data);
			}
		}
		else
		{
			$data['error'] = 0; //Error None
			$this->load->view('admin/login',$data);
		}
	}
	
	public function dashboard()
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		if($username != '')
		{
			//$date = date('Y-m-d');
			/*$data['result0']=$this->AdminModel->Statuscheck($username);
			$data['todaynews']=$this->AdminModel->CountAllNewsToday($date);
			$data['uploadvideos']=$this->AdminModel->CountAllVideos();
			$data['uploadimages']=$this->AdminModel->CountAllImages();
			$data['subscriber']=$this->AdminModel->CountSubscriber();
			$data['feedback']=$this->AdminModel->CountFeedback();
			$data['users']=$this->AdminModel->CountAllUsers();*/
			$this->load->view('admin/dashboard');
		}
		else
		{
			redirect("admin");
		}
				
	}
	
	public function agentcreate()
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		if($username != '')
		{
			$data['msg'] = 0;
			$this->load->view('admin/agent_create', $data);
		}
		else
		{
			redirect("admin");
		}
	}
	
	public function insertagent()
	{
		if($_POST)
		{
			$this->load->library('session');
			$this->load->model('AdminModel');
			$username = $this->session->userdata('username');
			$result = $this->adminModel->getUserId();
			$aid='';
			foreach($result as $r)
			{
				$aid = $r->agentid;
			}
			$newstring = substr($aid, -3);
			$newstring1 = $newstring+1;
			$newstring2 = str_pad($newstring1, 4, "0", STR_PAD_LEFT);
			$rqno1="LBA";
			$agentid=$rqno1.$newstring2;
			$docpath1 ="uploads/agent/".$agentid."/photo/";
			$docpath2 ="uploads/agent/".$agentid."/supporteddoc/";
			if (file_exists($docpath1))
			{
			}
			else
			{
				mkdir($docpath1, 0777, TRUE);
			}
			if (file_exists($docpath2))
			{
			}
			else
			{
				mkdir($docpath2, 0777, TRUE);
			}
			if(!empty($_FILES['photo']['name'])){
                $config['upload_path'] = $docpath1;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['photo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('photo')){
                    $uploadData1 = $this->upload->data();
                    $picture1 = $uploadData1['file_name'];
					$upparh1 = $docpath1.$picture1;
                }else{
                    $upparh1 = '';
                }
            }else{
                $upparh1 = '';
            }
			if(!empty($_FILES['doc']['name'])){
                $config['upload_path'] = $docpath2;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['doc']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('doc')){
                    $uploadData2 = $this->upload->data();
                    $picture2 = $uploadData2['file_name'];
					$upparh2 = $docpath2.$picture2;
                }else{
                    $upparh2 = '';
                }
            }else{
                $upparh2 = '';
            }
			
			
			$data = array(
				'agentid' => $agentid,
				'password' => $this->input->post('password'),
				'aname' => $this->input->post('aname'),
				'gender' => $this->input->post('gender'),
				'phone' => $this->input->post('phone'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'aadhar_number' => $this->input->post('aadhar'),
				'pan_number' => $this->input->post('pan'),
				'photo' => $upparh1,
				'supported_doc' => $upparh2,
				'status' => $this->input->post('status'),
				'addby' => $username,
				'doc' => date('Y-m-d H:i:s'),
				'dom' => date('Y-m-d H:i:s'),
				'xdelete' => 0,
			);
			
			$this->AdminModel->AgentDetailsInsert($data);
			$data['msg'] = 1;
			$this->load->view('admin/agent_create', $data);
		}
		else
		{
			redirect(site_url('admin/agentcreate'));
		}
	}
	
	public function agentview()
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		if($username != '')
		{
			$data['agentdetails'] = $this->AdminModel->getAllAgentDetails();
			$this->load->view('admin/agent_view', $data);
		}
		else
		{
			redirect("admin");
		}
	}
	
	public function DeleteAgent($aid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$this->AdminModel->deleteThsiAgent($aid);
		redirect("admin/agentview");
	}
	
	public function ActiveAgent($aid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 0
		);
		$this->AdminModel->updateThsiAgent($aid,$data);
		redirect("admin/agentview");
	}
	
	public function InactiveAgent($aid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 1
		);
		$this->AdminModel->updateThsiAgent($aid,$data);
		redirect("admin/agentview");
	}
	
	public function allusers()
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		if($username != '')
		{
			$data['userdetails'] = $this->AdminModel->getAllUserDetails();
			$this->load->view('admin/all_users_details', $data);
		}
		else
		{
			redirect("admin");
		}
	}
	
	public function DeleteUser($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$this->AdminModel->deleteThsiUser($uid);
		redirect("admin/allusers");
	}
	
	public function ActiveUser($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 0,
		);
		$this->AdminModel->updateThsiUser($uid,$data);
		redirect("admin/allusers");
	}
	
	public function InactiveUser($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 1
		);
		$this->AdminModel->updateThsiUser($uid,$data);
		redirect("admin/allusers");
	}
	
	public function PartnerLogo()
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		if($username != '')
		{
			$data['msg'] = 0;
			$data['alllogo'] = $this->AdminModel->getAllLogoDetails();
			$this->load->view('admin/add_partner_logo', $data);
		}
		else
		{
			redirect("admin");
		}
	}
	
	public function insertlogo()
	{
		if($_POST)
		{
			$docpath ="uploads/partners/";
			if (file_exists($docpath))
			{
			}
			else
			{
				mkdir($docpath, 0777, TRUE);
			}
			if(!empty($_FILES['photo']['name'])){
                $config['upload_path'] = $docpath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif|JPG|JPEG|PNG';
                $config['file_name'] = $_FILES['photo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('photo')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
					$upparh = $docpath.$picture;
                }
				else
				{
                    $upparh = '';
                }
            }
			else
			{
				//$error = array('error' => $this->upload->display_errors());
				//print_r($error);
                $upparh = '';
            }
			//echo $upparh;
			//exit;
			$datai = array(
				'client_name' => $this->input->post('cname'),
				'logo' => $upparh,
				'doc' => date('Y-m-d H:i:s'),
				'dom' => date('Y-m-d H:i:s'),
				'status' => 0,
				'xdelete' => 0,
			);
			$this->AdminModel->InsertLogoDetails($datai);
			$data['msg'] = 1;
			$data['alllogo'] = $this->AdminModel->getAllLogoDetails();
			$this->load->view('admin/add_partner_logo', $data);
		}
		else
		{
			redirect(site_url('admin/PartnerLogo'));
		}
	}
	
	public function DeleteLogo($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$this->AdminModel->deleteThsiLogo($uid);
		redirect("admin/PartnerLogo");
	}
	
	public function ActiveLogo($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 0,
			'dom' => date('Y-m-d H:i:s')
		);
		$this->AdminModel->updateThsiLogo($uid,$data);
		redirect("admin/PartnerLogo");
	}
	
	public function InactiveLogo($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 1,
			'dom' => date('Y-m-d H:i:s')
		);
		$this->AdminModel->updateThsiLogo($uid,$data);
		redirect("admin/PartnerLogo");
	}
	
	public function TeamAdd()
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		if($username != '')
		{
			$data['msg'] = 0;
			$data['teamdetails'] = $this->AdminModel->getAllTeamDetails();
			$this->load->view('admin/add_team', $data);
		}
		else
		{
			redirect("admin");
		}
	}
	
	public function insertteammember()
	{
		if($_POST)
		{
			$docpath ="uploads/team/";
			if (file_exists($docpath))
			{
			}
			else
			{
				mkdir($docpath, 0777, TRUE);
			}
			if(!empty($_FILES['photo']['name'])){
                $config['upload_path'] = $docpath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif|JPG|JPEG|PNG';
                $config['file_name'] = $_FILES['photo']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('photo')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
					$upparh = $docpath.$picture;
                }
				else
				{
                    $upparh = '';
                }
            }
			else
			{
				//$error = array('error' => $this->upload->display_errors());
				//print_r($error);
                $upparh = '';
            }
			//echo $upparh;
			//exit;
			$datai = array(
				'member_name' => $this->input->post('cname'),
				'position' => $this->input->post('position'),
				'email' => $this->input->post('email'),
				'mobile' => $this->input->post('mobile'),
				'google_plus' => $this->input->post('google_plus'),
				'facebook' => $this->input->post('facebook'),
				'twitter' => $this->input->post('twittwe'),
				'photo' => $upparh,
				'doc' => date('Y-m-d H:i:s'),
				'dom' => date('Y-m-d H:i:s'),
				'status' => 0,
				'xdelete' => 0,
			);
			$this->AdminModel->InsertTeamDetails($datai);
			$data['msg'] = 1;
			$data['teamdetails'] = $this->AdminModel->getAllTeamDetails();
			$this->load->view('admin/add_team', $data);
		}
		else
		{
			redirect(site_url('admin/TeamAdd'));
		}
	}
	
	public function DeleteMember($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$this->AdminModel->deleteThsiMember($uid);
		redirect("admin/TeamAdd");
	}
	
	public function ActiveMember($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 0,
			'dom' => date('Y-m-d H:i:s')
		);
		$this->AdminModel->updateThsiMember($uid,$data);
		redirect("admin/TeamAdd");
	}
	
	public function InactiveMember($uid)
	{
		$this->load->library('session');
		$this->load->model('AdminModel');
		$username = $this->session->userdata('username');
		$data = array(
			'status' => 1,
			'dom' => date('Y-m-d H:i:s')
		);
		$this->AdminModel->updateThsiMember($uid,$data);
		redirect("admin/TeamAdd");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		//$this->load->view("admin_login");
		redirect("admin");
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */