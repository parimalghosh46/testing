<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->library('session');
        $this->load->helper(array('form', 'url'));
		$this->load->model('AdminModel');
		$this->load->model('common_m');
		$this->load->model('user_m');
    }
	public function index()
	{
		$data['alllogo'] = $this->common_m->getLogoDetails();
		$data['teammembers'] = $this->common_m->getAllTeamMembers();
		$this->load->view('home/homepage', $data);
	}

	public function selectusertype()
	{
		$this->load->library('session');		
		$this->load->model('user_m');
		/*$username = $this->session->userdata('username');
		if($username !="")
		{
			$data['userdetails'] = $this->user_m->getUserDetails($username);
		}
		else
		{
			$data['userdetails'] = '';
		}*/
		$this->load->view('home/usertypeselect');
	}






	
	
	public function Profile()
	{
		$this->load->library('session');
		$this->load->model('user_m');
		$username = $this->session->userdata('username');
		if($username !="")
		{
			$data['userdetails'] = $this->user_m->getUserDetails($username);
		}
		else
		{
			$data['userdetails'] = '';
		}
		
		$this->load->view('home/profile',$data);
	}
	
	
	public function Login()
	{
		if(isset($_POST['username']) && isset($_POST['password']))
		{
			$this->load->library('session');
			$user=$this->input->post('username');
			$pass=$this->input->post('password');
			$this->load->model('UserModel');
			if($user !='' && $pass !='')
			{
				$result=$this->user_m->usrerlogin($user,$pass);
				$id="";
				$uname="";
				$name="";
				$date="";
				$user_status="";
				foreach ($result as $r)
				{
					$id=$r->id;
					$uname = $r->username;
					$name = $r->name;
					$date = $r->lastlogintime;
					$user_status = $r->user_status;
				}
				if($user_status =="admin")
				{
					$data= array(
						'id'=> $id,
						'username'=> $uname,
						'lastlogintime'=>$date,
						'name'=>$name,
						'user_status'=>$user_status,
					);
					$datetime = date('Y-m-d h:i:s a', time());
					$this->session->set_userdata($data);
					$userid = $this->session->userdata('id');
					$name = $this->session->userdata('name');
					$username = $this->session->userdata('username');
					$user_status = $this->session->userdata('user_status');
					$dt = $this->session->userdata('lastlogintime');
					$this->user_m->LastLoginTimeUpdate($id,$datetime);
					redirect("Admin/Dashboard");
				}
				else if($user_status =="user")
				{
					$data= array(
						'id'=> $id,
						'username'=> $uname,
						'lastlogintime'=>$date,
						'name'=>$name,
						'user_status'=>$user_status,
					);
					$datetime = date('Y-m-d h:i:s a', time());
					$this->session->set_userdata($data);
					$userid = $this->session->userdata('id');
					$name = $this->session->userdata('name');
					$username = $this->session->userdata('username');
					$user_status = $this->session->userdata('user_status');
					$dt = $this->session->userdata('lastlogintime');
					$this->user_m->LastLoginTimeUpdate($id,$datetime);
					redirect("home");
				}
				else
				{
					redirect('home/login?s=1');
				}
			}
			else
			{
				redirect('home/login?s=1');
			}
			
		}
		else
		{
			$this->load->view('home/login');
		}
	}
	
	
	public function InsertnewTopic()
	{
		$this->load->library('session');
		$this->load->model('user_m');
		$userid = $this->session->userdata('id');
		$topic = $_POST['val'];
		$data = array(
				'topicname' => $topic,
				'ststus' => 0,
				'views' => 0,
				'addby' => $userid,
				'doc' => date('Y-m-d H:i:S'),
				'xdelete' => 0
			);
		$id = $this->user_m->InsertnewTopic($data);
		if($id != "")
		{
			echo "0";
		}
		else
		{
			echo "1";
		}
	}
	
	public function ShowallTopic()
	{
		$this->load->library('session');
		$this->load->model('user_m');
		$userid = $this->session->userdata('id');
		$topic = $this->user_m->getAllTopic();
		echo "<h4><b>Trends for you. </b><span style='font-size:12px;color:red;'><a  style='color:red;text-decoration:none;' href='#'>Change</a></span></h4>";
		foreach($topic as $top)
		{
			echo "<h4><span style='color:red;font-width:bold;'><a  style='color:red;text-decoration:none;' href='#'>".$top->topicname."</a></span><p><span style='font-size:12px;'>".$top->views."</span></p></h4>";
		}
		
	}
	
	public function ShowallPostDetails()
	{
		$this->load->library('session');
		$this->load->model(user_m);
		$userid = $this->session->userdata('id');
		$topic_post = $this->user_m->getAllTopicPost();
		$userdetails = $this->user_m->getUserDetails12();
		foreach($topic_post as $tp)
		{
			echo "<div class='timeline-item'>
                    <div class='timeline-item-content'>
                        <div class='timeline-heading'>";
						foreach($userdetails as $ur)
						{
							if($ur->id == $tp->add_by){
            echo"<img src='".base_url()."/uploads/profile/".$ur->profile_pic."'> <a href='#'>John Doe</a> added article <a href='#'>Lorem ipsum dolor sit amet</a>";	
							}
						}
            echo "</div>
                        <div class='timeline-body'>
                        </div>
                        <div class='timeline-body comments'>
                            <div class='comment-item'>"
                                .$tp->post_details.
                                "<small class='text-muted'>Date : ".$tp->post_date."</small>
                            </div>                                            
							<div class='comment-write'>                                                
								<textarea class='form-control' placeholder='Write a comment' rows='1'></textarea>                                                
                            </div>
                        </div>                                        
                        <div class='timeline-footer'>
                            <a href='#'>Read more</a>
                            <div class='pull-right'>
                                <a href='#'><span class='fa fa-comment'></span> 35</a> 
                                <a href='#'><span class='fa fa-share'></span></a>
                            </div>
						</div>
                    </div>
                </div>";
		}
		
	}
	
	public function InsertaPOST()
	{
		$this->load->library('session');
		$this->load->model('user_m');
		$userid = $this->session->userdata('id');
		$post_details = $_POST['post_details'];
		
		$data = array(
				'post_id' => uniqid(),
				'post_details' => $post_details,
				'post_time' => date('H:i:s'),
				'post_date' => date('Y-m-d'),
				'status' => 0,
				'views' => 0,
				'add_by' => $userid,
				'doc' => date('Y-m-d H:i:S'),
				'xdelete' => 0
			);
		$id = $this->user_m->InsertnewPost($data);
		redirect('home');
	}
	
	/*public function insert()
	{
		echo $_POST['post_details'];
	}*/
	
	public function PrivacyPolicy()
	{
		$this->load->view('home/privacypolicy');
	}
	
	public function TermsandConditions()
	{
		$this->load->view('home/termsandconditions');
	}
	
	public function sendmessage()
	{
		$this->load->model('user_m');
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'subject' => $this->input->post('subject'),
			'message' => $this->input->post('message'),
			'doc' => date('Y-m-d H:i:s'),
			'status' => 0,
			'xdelete' => 0,
		);
		$this->common_m->InsertQuery($data);
		echo "<script>alert('Successfully Submitted Query!');window.location.href='".base_url()."';</script>";
	}
	
	public function NewApplication()
	{
		$this->load->view('home/new_application');
	}
	
	public function userapplication()
	{
		if($_POST)
		{
			$this->load->model('common_m');
			$aap = rand(10000, 100000);
			$aapno = 'LBA/'.date('y').'/'.$aap;
			
			$data = array(
				'uname' => $this->input->post('fname').' '.$this->input->post('lname'),
				'coname' => $this->input->post('coname'),
				'dob' => $this->input->post('dob'),
				'gender' => $this->input->post('gender'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'address' => $this->input->post('address'),
				'doc' => date('Y-m-d H:i:s'),
				'application_for' => $this->input->post('application_for'),
				'application_no' => $aapno,
				'status' => 0,
				'xdelete' => 0,
			);
			$this->common_m->InsertNewAppliacion($data);
			
			$this->session->set_flashdata('msg', 'Succesfully Applied! Your Application id '.$aapno);
			redirect("home/NewApplication");
		}
	}
	
	public function CheckApplicationStatus()
	{
		if($_POST)
		{
			$appid = $this->input->post('application_no');
			$dob = $this->input->post('dob');
			$status = $this->common_m->getApplicationStatus($appid,$dob);
			if(COUNT($status) > 0)
			{
				$data['status'] = $status;
				$data['view'] = 0;				
			}
			else
			{
				$data['status'] = array();
				$data['view'] = 1;
			}
		}
		else
		{
			$data['status'] = array();
			$data['view'] = 2;
		}
		$this->load->view('home/application_status', $data);
	}
	
	public function checkAppStatus()
	{
		//if($_POST)
		//{
			$appid = $this->input->post('application_no');
			$dob = $this->input->post('dob');
			$status = $this->common_m->getApplicationStatus($appid,$dob);
			if(COUNT($status) > 0)
			{
				echo '<div style="border: 2px solid black;padding: 20px;border-radius: 10px;">
							<div class="form-group">
								<label style="font-size: 25px;"><b>Application Number : </b> <span>'.$status[0]->application_no.'</span></label> 
							</div>
							<div class="form-group">
								<label style="font-size: 25px;"><b>Name : </b><span>'.$status[0]->uname.'</span></label> 
							</div>
							<div class="form-group">
								<label style="font-size: 25px;"><b>Status : </b><span>'.$status[0]->status.'</span></label> 
							</div>
							</div>';
			}
			else
			{
				echo '<div style="border: 2px solid black;padding: 20px;border-radius: 10px;">
							<h2 style="color:red;">Incorrect Input!</h2>
							</div>';
			}
		//}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect("home");
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */