<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent extends CI_Controller {
	
	public function index()
	{
		$this->load->library('session');
		$this->load->model('agent_m');
		$data['error'] = 0; //Error None
		$this->load->view('agent/login',$data);
	}
	
	public function login()
	{
		$this->load->library('session');
		$this->load->model('agent_m');
		if($_POST)
		{
			$user=$this->input->post('username');
			$pass=$this->input->post('password');
			if($user !='' && $pass !='')
			{
				$result=$this->agent_m->agentlogin($user,$pass);
				if(COUNT($result)>0)
				{
					$aid="";
					$aname="";
					$date="";
					$status="";
					foreach ($result as $r)
					{
						$aid=$r->agentid;
						$aname=$r->agentname;
						//$date=$r->lastlogintime;
						$status=$r->status;
					}
					
					$data= array(
						'agentid'=> $aid,
						'agentname'=> $aname,
						//'lastlogintime'=>$date,
						'status'=>$status,
					);
					$datetime = date('Y-m-d h:i:s a', time());
					$this->session->set_userdata($data);
					$agentid = $this->session->userdata('agentid');
					$agentname = $this->session->userdata('agentname');
					$status = $this->session->userdata('status');
					//$dt= $this->session->userdata('lastlogintime');
					//$this->AdminModel->LastLoginTimeUpdate($id,$datetime);
					redirect("agent/agenthome");
				}
				else
				{
					$data['error'] = 1; //Userid & Password Incorrect
					$this->load->view('agent/login',$data);
				}
			}
			else
			{
				$data['error'] = 2; //Userid & Password Blank
				$this->load->view('agent/login',$data);
			}
		}
		else
		{
			$data['error'] = 0; //Error None
			$this->load->view('agent/login',$data);
		}
	}
	
}

?>