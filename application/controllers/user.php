<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	
	public function login()
	{
		$this->load->library('session');
		$this->load->model('user_m');
		if($_POST)
		{
			$user=$this->input->post('username');
			$pass=$this->input->post('password');
			if($user !='' && $pass !='')
			{
				$result=$this->user_m->userlogin($user,$pass);
				if(COUNT($result)>0)
				{
					$id="";
					$name="";
					$date="";
					$status="";
					foreach ($result as $r)
					{
						$id=$r->userid;
						$name=$r->username;
						//$date=$r->lastlogintime;
						$status=$r->status;
					}
					$data= array(
							'slno'=> $id,
							'username'=> $name,
							//'lastlogintime'=>$date,
							'status'=>$status,
					);
					$datetime = date('Y-m-d h:i:s a', time());
					$this->session->set_userdata($data);
					$userid = $this->session->userdata('slno');
					$username = $this->session->userdata('username');
					$status = $this->session->userdata('status');
					//$dt= $this->session->userdata('lastlogintime');
					//$this->AdminModel->LastLoginTimeUpdate($id,$datetime);
					redirect("user/userhome");
				}
				else
				{
					$data['error'] = 1; //Userid & Password Incorrect
					$this->load->view('user/login',$data);
				}
			}
			else
			{
				$data['error'] = 2; //Userid & Password Blank
				$this->load->view('user/login',$data);
			}
		}
		else
		{
			$data['error'] = 0; //Error None
			$this->load->view('user/login',$data);
		}
	}
	
	
}

?>